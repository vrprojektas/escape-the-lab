﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class LiquidAbsorption : MonoBehaviour {

    //public int collisionCount = 0;
    [SerializeField] List<string> tagsIgnore = new string[] { "Fire", "DrySmoke" }.ToList<string>();
    public Color currentColor;
    public BottleSmash smashScript;
    public LiquidLevel level;
    [SerializeField] ParticleColor particleColor;
    public float particleValue = 0.02f;
    //public LiquidVolumeAnimator LVA;

    // Use this for initialization
    void Start () {
        //if(LVA == null)
        //LVA = GetComponent<LiquidVolumeAnimator>();
	}
    void OnParticleCollision(GameObject other)
    {
        //check if it is the same factory.
        if (!tagsIgnore.Contains(other.tag))
        {
            if (other.transform.parent == transform.parent)
                return;
            bool available = false;
            if (smashScript.Cork == null)
            {
                available = true;
            }
            else
            {
                //if the cork is not on!
                if (!smashScript.Cork.activeSelf)
                {

                    available = true;
                }
                //or it is disabled (through kinamism)? is that even a word?
                else if (!smashScript.Cork.GetComponent<Rigidbody>().isKinematic)
                {
                    available = true;
                }
            }
            if (available)
            {
                currentColor = smashScript.color;
                if (level.level < 1.0f - particleValue)
                {

                    //essentially, take the ratio of the bottle that has liquid (0 to 1), then see how much the level will change, then interpolate the color based on the dif.
                    Color impactColor;

                    if (other.GetComponentInParent<BottleSmash>() != null)
                    {
                        impactColor = other.GetComponentInParent<BottleSmash>().color;
                    }
                    else
                    {
                        impactColor = other.GetComponent<ParticleSystem>().GetComponent<Renderer>().material.GetColor("_TintColor");
                    }

                    if (level.level <= float.Epsilon * 10)
                    {
                        currentColor = impactColor;
                    }
                    else
                    {
                        currentColor = Color.Lerp(currentColor, impactColor, particleValue / level.level);
                    }
                    //collisionCount += 1;
                    level.level += particleValue;
                    smashScript.color = currentColor;
                }
            }
        }

    }
	// Update is called once per frame
	void Update ()
	{
        smashScript.ChangedColor();
        currentColor = smashScript.color;
        particleColor.Unify();
    }
}
