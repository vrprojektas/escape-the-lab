﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class FallingWater : MonoBehaviour
{
    public bool Active = false;
    public GameObject particle;
    [SerializeField] AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.rotation.eulerAngles.x >= 340)
        {
            particle.GetComponent<ParticleSystem>().Play();
            if (!audio.isPlaying)
            { 
                audio.Play();
            }
            Active = true;
        }
        else
        {
            particle.GetComponent<ParticleSystem>().Stop();
            audio.Stop();
            Active = false;
        }
    }
}
