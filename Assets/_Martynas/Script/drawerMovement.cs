﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drawerMovement : MonoBehaviour
{
    [SerializeField] float audioVolume = 0.09f;

    float position;
    int timer;
    [SerializeField] AudioSource audio;
    public float fadeTime = 1;
    float t;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(muteSound());
        position = transform.position.x;
        t = fadeTime;
    }

    // Update is called once per frame
    void Update()
    {
        float newposition = transform.localPosition.x - position;
        if (newposition > 0.007 || newposition < -0.007)
        {
            if (!audio.isPlaying)
            {
                audio.Play();
            }
        }
        else if(audio.isPlaying)
        {
            if (newposition < 0.007 || newposition > -0.007)
            {
                    t -= Time.deltaTime;
                    audio.volume = t / fadeTime;
                    if (t <= 0)
                    {
                        t = fadeTime;
                        audio.Stop();
                    }
            }
            else
            {
                t = fadeTime;
            }
        }
        position = transform.localPosition.x;

    }

    IEnumerator muteSound()
    {
        audio.volume = 0;
        yield return new WaitForSeconds(5);
        audio.volume = audioVolume;
    }
}
