﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinkSound : MonoBehaviour
{
    [SerializeField] AudioSource audio;
    int scheck;
    int check;

    // Start is called before the first frame update
    void Start()
    {
        check = 0;
        scheck = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (check != scheck)
        {
            scheck = check;
        }
        else
        {
            audio.Stop();
        }
        
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.tag == "Water")
        {
            check++;
            if (!audio.isPlaying)
            {
                audio.Play();
            }
        }
    }
}
