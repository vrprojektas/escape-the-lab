﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables.PhysicsBased;

public class ShelfSound : MonoBehaviour
{
    [SerializeField] float audioVolume = 0.35f;

    float position;
    float checkposition;
    bool sound;
    float timer;
    float timercheck;
    bool timerbool;
    [SerializeField] AudioSource audio;
    [SerializeField] VRTK_PhysicsRotator pr;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(muteSound());
        position = 0;
        timercheck = 0;
        sound = true;
        timerbool = false;
        timer = 0.4f;
    }

    // Update is called once per frame
    void Update()
    {

        timer -= Time.deltaTime*2;
        timercheck = timercheck + Mathf.Abs(checkposition-pr.previousValue);
        if (timer < 0)
        {
            if (timercheck < 15)
            {
                timer = 0.4f;
                timerbool = false;
                timercheck = 0;
            }
            else
            {
                timer = 0.4f;
                timerbool = true;
                timercheck = 0;
            }
        }
        checkposition = pr.previousValue;
        if (position == checkposition && sound == false && !audio.isPlaying && timerbool == true)
        {
            audio.Play();
            sound = true;
        }
        else if (position != checkposition)
        {
            sound = false;
        }
    }

    IEnumerator muteSound()
    {
        audio.volume = 0;
        yield return new WaitForSeconds(5);
        audio.volume = audioVolume;
    }

}
