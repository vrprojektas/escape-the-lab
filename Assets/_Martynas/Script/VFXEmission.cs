﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.VFX;


public class VFXEmission : MonoBehaviour
{
    public int count;
    UnityEngine.Experimental.VFX.VisualEffect vfx;
    // Start is called before the first frame update
    void Start()
    {
        vfx = this.GetComponent<UnityEngine.Experimental.VFX.VisualEffect>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DryIce")
        {
            count++;
            vfx.SetInt("SpawnRate", count);
            if (count == 0)
            {
                count = 3;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "DryIce")
        {
            count--;
            vfx.SetInt("SpawnRate", count);
            if (count < 3)
            {
                count = 0;
            }
        }
    }
}
