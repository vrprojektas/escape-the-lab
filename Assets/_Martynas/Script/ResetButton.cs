﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;

public class ResetButton : MonoBehaviour
{
    [SerializeField] VRTK_BaseControllable controllable;
    [SerializeField] LiquidLevel LVA;
    [SerializeField] ObjectCheck aluminium;
    [SerializeField] MixingScript mix;
    [SerializeField] float countTime = 1;
    [SerializeField] int chunks = 1;
    ButtonSound sound;
    float time = 0;
    float chunkCnt = 0;
    float chunkLevel = 0;
    bool playAnimation = false;
    
    // Start is called before the first frame update
    void Start()
    {
        sound = GetComponent<ButtonSound>();
    }

    protected virtual void OnEnable()
    {
        controllable = (controllable == null ? GetComponent<VRTK_BaseControllable>() : controllable);
        controllable.MinLimitReached += MaxLimitReached;
    }

    // Update is called once per frame
    protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        chunkLevel = LVA.level / chunks;
        playAnimation = true;
        mix.ResetMix();
        aluminium.ResetObcect();
        sound.check = true;
    }

    private void Update()
    {
        if (playAnimation)
        {
            if (time < countTime / chunks && chunkCnt < chunks)
            {
                time += Time.deltaTime;
            }
            else if (time >= countTime / chunks && chunkCnt < chunks)
            {
                time = 0;
                chunkCnt++;
                float level = LVA.level - chunkLevel;
                LVA.level = level;
            }
            if (LVA.level <= 0.0006)
            {
                chunkCnt = 0;
                playAnimation = false;
            }
        }
    }

}
