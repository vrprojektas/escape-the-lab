﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.VFX;

public class VFXTrackingHands : MonoBehaviour
{

    [SerializeField] List<GameObject> rightHand;
    [SerializeField] List<GameObject> leftHand;
    GameObject right;
    GameObject left;
    UnityEngine.Experimental.VFX.VisualEffect vfx;
    bool found = false;
    // Start is called before the first frame update
    void Start()
    {
        vfx = this.GetComponent<UnityEngine.Experimental.VFX.VisualEffect>();
    }

    // Update is called once per frame
    void Update()
    {

        if (!found)
        {
            
            for (int i = 0; i < rightHand.Count; i++)
            {
                if (rightHand[i].activeSelf && rightHand[i].activeInHierarchy)
                {
                    right = rightHand[i];

                }
                if (leftHand[i].activeSelf && leftHand[i].activeInHierarchy)
                {
                    left = leftHand[i];
                }
                if (left != null && right != null)
                {
                    found = true;
                }
            }
        }
        else
        {

            Vector3 tempRight = transform.InverseTransformPoint(right.transform.position);
            Vector3 tempLeft = transform.InverseTransformPoint(left.transform.position);
            vfx.SetVector3("Right", tempRight);
            vfx.SetVector3("Left", tempLeft);
        }
    }
}
