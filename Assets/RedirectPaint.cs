﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectPaint : MonoBehaviour
{
    public PaintTarget redirectTarget;
    private void Start()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<PaintTarget>() != null)
            {
                redirectTarget = transform.GetChild(i).GetComponent<PaintTarget>();
            }
        }
    }
}
