﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTime : MonoBehaviour
{
    public LockUnlockWithKey locks;
    [SerializeField] TimeLeft time;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (locks.isChestOpen())
        {
            time.StopTimer();
            time.finalStop = true;
        }
    }
}
