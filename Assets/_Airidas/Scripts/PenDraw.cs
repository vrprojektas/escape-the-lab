﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenDraw : MonoBehaviour
{

    [SerializeField] Color color;
    [Range(1,10)]
    [SerializeField] float brushStrenght;
    [SerializeField] Vector2 offset;

    public Color GetColor()
    {
        return color;
    }

    public float GetBrushStrenght()
    {
        return brushStrenght;
    }

    public Vector2 GetOffset()
    {
        return offset;
    }


}
