﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class TimeLeft : MonoBehaviour
{
    [SerializeField] TextMeshPro timerMinutes;
    [SerializeField] TextMeshPro timerSeconds;
    private float stopTime;
    public float timerTime;

    public bool finalStop = false;
    private bool isRunning = false;

    private void Start()
    {
        StopTimer();
    }

    private void Update()
    {
        if (isRunning && !finalStop)
        {
            timerTime = stopTime + (timerTime - Time.deltaTime);
            int minutesInt = (int)timerTime / 60;
            int secondsInt = (int)timerTime % 60;
            timerMinutes.text = (minutesInt < 10) ? "0" + minutesInt : minutesInt.ToString();
            timerSeconds.text = (secondsInt < 10) ? "0" + secondsInt : secondsInt.ToString();
            if(timerTime <= 0)
            {
                GameData.SetEnd(true);
                GameData.SetVictory(false);
                isRunning = false;
            }
        }
    }

    public void StopTimer()
    {
        isRunning = false;
    }

    public void TimerStart()
    {
        if(!isRunning)
        {
            print("timer is running");
            isRunning = true;
        }
    }

    public void MinusTime(float seconds)
    {
        timerTime -= seconds;
    }

}
