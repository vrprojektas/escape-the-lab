﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperBurn : MonoBehaviour
{
    [SerializeField] GameObject fire;
    [SerializeField] GameObject smoke;

    [SerializeField] float melting = 0.1f;
    [SerializeField] string particleTag;

    [SerializeField] float speed = 1.0f;
    [SerializeField] Color startColor;
    [SerializeField] Color endColor;
    float startTime;
    float temp;

    [SerializeField] Renderer rend;

    bool changedColor = false;
    public bool isBurning = false;

    private void Start()
    {
        fire.SetActive(false);
        smoke.SetActive(false);
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.tag == particleTag)
        {
            fire.SetActive(true);
            isBurning = true;
            paperBurning();
            smoke.SetActive(false);
        }
        else if (other.tag == "Water" && isBurning)
        {
            Invoke("StopBurning", 0.05f);
            isBurning = false;
            smoke.SetActive(true);
        }
    }

    private void Update()
    {
        if (fire.activeInHierarchy && !changedColor)
        {
            startTime = Time.time;
            changedColor = true;
        }

        if (fire.activeInHierarchy)
        {
            paperBurning();
            temp = (Time.time - startTime) * speed;
            rend.material.color = Color.Lerp(startColor, endColor, temp);
        }
    }

    void StopBurning()
    {
        fire.SetActive(false);
    }

    private void paperBurning()
    {
        if(isBurning)
        {
            if (this.transform.localScale.x > 0.05 && this.transform.localScale.y > 0.05 && this.transform.localScale.z > 0.05)
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x < 0.05 && this.transform.localScale.y > 0.05 && this.transform.localScale.z > 0.05) // x < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x > 0.05 && this.transform.localScale.y < 0.05 && this.transform.localScale.z > 0.05) // y < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x > 0.05 && this.transform.localScale.y > 0.05 && this.transform.localScale.z < 0.05) // z < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
            }

            // Two checks

            else if (this.transform.localScale.x < 0.05 && this.transform.localScale.y < 0.05 && this.transform.localScale.z > 0.05) // x y < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x < 0.05 && this.transform.localScale.y > 0.05 && this.transform.localScale.z < 0.05) // x z < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
            }
            else if (this.transform.localScale.x > 0.05 && this.transform.localScale.y < 0.05 && this.transform.localScale.z < 0.05) // y z < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z);
            }

            if (this.transform.localScale.z < 0.05 && this.transform.localScale.x < 0.05 && this.transform.localScale.y < 0.05)
            {
                Destroy(gameObject);
            }
        }
    }
}
