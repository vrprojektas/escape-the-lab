﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AntiAliasing : MonoBehaviour
{
    TMP_Dropdown dropdown;

    void Start()
    {
        dropdown = GetComponent<TMP_Dropdown>();
    }

    private void OnEnable()
    {
        if (dropdown != null)
        {
            switch (QualitySettings.antiAliasing)
            {
                case 0:
                    dropdown.value = 0;
                    break;
                case 2:
                    dropdown.value = 1;
                    break;
                case 4:
                    dropdown.value = 2;
                    break;
                default:
                    dropdown.value = 3;
                    break;
            }
        }
    }

    public void SetAntiAliasing(int index)
    {
        switch (dropdown.value)
        {
            case 0:
                index = 1;
                break;
            case 1:
                index = 2;
                break;
            case 2:
                index = 4;
                break;
            default:
                index = 8;
                break;
        }
        QualitySettings.antiAliasing = index;
        UpdateSettings.Save = true;
    }
    
}
