﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teleports : MonoBehaviour
{
    TimeLeft timeleft;

    private void Start()
    {
        timeleft = GetComponent<TimeLeft>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            restartLevel();
            GameData.Cear();
        }
        if (GameData.End == true && GameData.Victory == false)
        {
            loseScene();
            GameData.Cear();
        }
    }

    void loseScene()
    {
        SceneManager.LoadScene(3);
    }

    void restartLevel()
    {
        SceneManager.LoadScene(2);
    }
}
