﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.Controllables.PhysicsBased;

public class ChestInteraction : MonoBehaviour
{
    private int collisionCount = 0;
    private bool rigidbodyExists = false;

    public float thrust = 2f;
    [SerializeField] GameObject go;
    private Rigidbody rb;

    private VRTK_SnapDropZone snapZone;
    private LockUnlockWithKey lockUnlock;

    private void Start()
    {
        go = gameObject.transform.GetChild(14).gameObject;
        snapZone = gameObject.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<VRTK_SnapDropZone>();
        lockUnlock = GetComponent<LockUnlockWithKey>();
    }

    private void Update()
    {
        if (lockUnlock.isChestOpen() && snapZone.IsSnapped() && collisionCount < 1)
        {
            collisionCount = 1;
            rb = go.GetComponent<Rigidbody>();
            rb.AddForce(transform.up * thrust);
        }
    }

}
