﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningFlask : MonoBehaviour
{
    [SerializeField] Animator anim;
    bool animatorActive = false;

    void Start()
    {
        anim.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "[VRTK][AUTOGEN][Controller][NearTouch][CollidersContainer]" && !animatorActive)
        {
            anim.enabled = true;
        }
    }
}
