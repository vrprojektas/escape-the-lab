﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperBurnSound : MonoBehaviour
{
    [SerializeField] AudioSource fireAud;
    [SerializeField] AudioSource smokeAud;

    [SerializeField] GameObject fire;
    PaperBurn paperBurn;

    bool isPlaying = false;

    // Start is called before the first frame update
    void Start()
    {
        paperBurn = GetComponent<PaperBurn>();
    }

    // Update is called once per frame
    void Update()
    {
        if(fire.activeInHierarchy && !isPlaying && paperBurn.isBurning)
        {
            fireAud.Play();
            isPlaying = true;
        }
        else if(!paperBurn.isBurning && isPlaying)
        {
            fireAud.Stop();
            isPlaying = false;
            smokeAud.Play();
        }
    }
}
