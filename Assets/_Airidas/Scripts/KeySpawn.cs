﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeySpawn : MonoBehaviour
{
    public GameObject key;
    public GameObject Flask;

    int count;

    private void Start()
    {
        count = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(Flask == null && count == 1)
        {
            InstantiateKey();
        }
    }

    private void InstantiateKey()
    {
        Instantiate(key, transform.position, transform.rotation);
        Debug.Log("Key spawned!");
        count++;
    }
}
