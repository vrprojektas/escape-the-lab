﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WindowUI : MonoBehaviour
{
    TMP_Dropdown dropdown;

    private void OnEnable()
    {
        if (dropdown == null)
        {
            dropdown = GetComponent<TMP_Dropdown>();
        }
        if (Screen.fullScreen)
        {
            dropdown.value = 0;
        }
        else
        {
            dropdown.value = 1;
        }
    }

    public void SetFullscreen(int isFullscreen)
    {
        if (isFullscreen == 0)
        { 
            Screen.fullScreen = true;
            StartResolutionUi.fullScreen = true;
        }
        else if (isFullscreen == 1)
        {
            Screen.fullScreen = false;
            StartResolutionUi.fullScreen = false;
        }

        UpdateSettings.Save = true;
    }
}
