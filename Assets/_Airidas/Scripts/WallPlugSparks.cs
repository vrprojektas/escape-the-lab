﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class WallPlugSparks : MonoBehaviour
{
    //TODO fix animation component disabling/enabling
    [SerializeField] List<GameObject> lights;
    [SerializeField] Vector2 RandonTimeFromTo = Vector2.zero;
    [SerializeField] ParticleSystem particle;
    public GameObject[] litObjs;
    public GameObject[] unlitObjs;
    List<float> timeToSpark;
    List<float> timeCount;
    List<bool> checkSparkle;
    bool spark = false;
    int lightBlinked = 0;
    AudioSource aud;
    VRTK_InteractableObject intercat;
    private void Start()
    {
        intercat = gameObject.GetComponent<VRTK_InteractableObject>();
        timeToSpark = new List<float>();
        timeCount = new List<float>();
        checkSparkle = new List<bool>();
        for (int i = 0; i < lights.Count; i++)
        {
            timeToSpark.Add(Random.Range(RandonTimeFromTo.x, RandonTimeFromTo.y));
            checkSparkle.Add(false);
            timeCount.Add(0);
        }
        
        aud = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "GameController" && other.name != "[VRTK][AUTOGEN][Controller][NearTouch][CollidersContainer]" && other != null)
        {
            particle.Play();
            aud.Play();
            spark = true;
            for (int i = 0; i < timeToSpark.Count; i++)
            {
                timeToSpark[i] = Random.Range(RandonTimeFromTo.x, RandonTimeFromTo.y);
            }
        }
    }

    private void Update()
    {
        if (intercat.IsTouched() && spark == false)
        {
            particle.Play();
            aud.Play();
            spark = true;
            for (int i = 0; i < timeToSpark.Count; i++)
            {
                timeToSpark[i] = Random.Range(RandonTimeFromTo.x, RandonTimeFromTo.y);
            }
        }
        if (spark)
        {
            for (int i = 0; i < timeToSpark.Count; i++)
            {
                if (timeToSpark[i] >= timeCount[i])
                {
                    litObjs[i].SetActive(false);
                    unlitObjs[i].SetActive(true);
                    lights[i].GetComponent<Light>().enabled = false;
                    timeCount[i] += Time.deltaTime;
                }
                else if (timeToSpark[i] < timeCount[i] && !checkSparkle[i])
                {
                    lightBlinked++;
                    litObjs[i].SetActive(true);
                    unlitObjs[i].SetActive(false);
                    lights[i].GetComponent<Light>().enabled = true;
                    checkSparkle[i] = true;
                }
            }
            if (lightBlinked >= timeToSpark.Count)
            {
                for (int i = 0; i < timeToSpark.Count; i++)
                {
                    if (lightBlinked == timeToSpark.Count)
                    {
                        checkSparkle[i] = false;
                        timeCount[i] = 0;
                    }
                }
                spark = false;
                lightBlinked = 0;
            }

        }
    }
}
