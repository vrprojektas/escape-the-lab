﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class DontDestroy : MonoBehaviour
{
    private static DontDestroy instance;
    private AudioSource aud;

    public AudioClip menuMusic;
    public AudioClip gameMusic;

    private void Start()
    {
        aud = GetComponent<AudioSource>();
    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
            instance = this;

        DontDestroyOnLoad(gameObject);
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "MainMenu")
        {
            if (aud == null)
                return;
            aud.Stop();
            aud.clip = menuMusic;
            aud.Play();
        }
        else if (scene.name == "MainGame")
        {
            if (aud == null)
                return;
            aud.Stop();
            aud.clip = gameMusic;
            aud.Play();
        }
    }

}
