﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.GrabAttachMechanics;
using VRTK.SecondaryControllerGrabActions;
public class VRTK_AutoSetGrab : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject setupObject;
    

    void Start()
    {
        
        foreach (Transform child in setupObject.transform)
        {
            VRTK_InteractableObject interact;
            interact = child.GetComponent<VRTK_InteractableObject>();
            interact.grabAttachMechanicScript = child.GetComponent<VRTK_FixedJointGrabAttach>();
            interact.secondaryGrabActionScript = child.GetComponent<VRTK_SwapControllerGrabAction>();
        }
        
        
    }

    // Update is called once per frame

}
