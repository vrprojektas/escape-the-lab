﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LightOn : MonoBehaviour
{
    [SerializeField] string TagForFire;
    ParticleSystem ps;
    MixingScript mix;
    [SerializeField] ObjectCheck aluminium;
    public bool Started = false;
    float temptime = 0;
    float time;

    void Start()
    {
        mix = GetComponent<MixingScript>();
        ps = gameObject.transform.parent.Find("Parent").GetComponent<ParticleSystem>();
        time = ps.main.duration;
    }

    void OnParticleCollision(GameObject other)
    {
        if (TagForFire == other.tag && !Started && mix.mixed && aluminium.added)
        {
            gameObject.transform.parent.Find("Parent").gameObject.SetActive(true);
            ps.Play();
            Started = true;
        }
    }

    void Update()
    {
        if (Started && time > temptime && mix.mixed && aluminium.added)
        {
            temptime += Time.deltaTime;
        }
        else if(Started && time <= temptime && mix.mixed && aluminium.added)
        {
            Started = false;
            temptime = 0;
        }
    }

}
