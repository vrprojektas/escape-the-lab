﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChameleonCheck : MonoBehaviour
{
    [SerializeField] string changeTagMixed = "SugarNaOHKMnO4";
    [SerializeField] string changeTagNoMixed = "SugarNaOH";
    [SerializeField] string tag;
    [SerializeField] MixingScript mix;
    [SerializeField] ParticleSystem ps;
    [SerializeField] GameObject material;
    [SerializeField] LiquidVolumeAnimator lva;
    public bool check;

    BottleSmash smash;
    bool change = false;
    int cnt = 0;
    GameObject go;
    Color color;

    private void Start()
    {
        mix = gameObject.GetComponent<MixingScript>();
        smash = gameObject.transform.parent.GetComponent<BottleSmash>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == tag)
        {
            
            go = collision.gameObject;
            go.GetComponent<Melting>().lva = lva;
            go.GetComponent<Melting>().OneTimeTrigger = true;
            collision.gameObject.GetComponent<BoxCollider>().isTrigger = true;
            color = lva.mats[0].GetColor("_Color");

        }
    }

    private void Update()
    {
        if (go != null && go.transform.position.y < lva.finalPoint.y && !check && mix.mixed)
        {

            ps.tag = changeTagMixed;
            check = true;
        }
        else if (go != null && go.transform.position.y < lva.finalPoint.y && !check && !mix.mixed)
        {
            ps.tag = changeTagNoMixed;

        }
        else if (go == null && !check && mix.mixed && ps.tag == changeTagMixed)
        {
            color = lva.mats[0].GetColor("_Color");
            check = true;
        }
        if (check && ps.tag == changeTagMixed)
        {
            if (!change)
            {
                if (cnt == 0)
                {
                    color.r -= 0.01f;
                    if (color.r <= 0)
                    {
                        color.r = 0;
                        change = true;
                        cnt++;
                    }
                }
                else if (cnt == 2)
                {
                    color.b -= 0.01f;
                    if (color.b <= 0)
                    {
                        color.b = 0;
                        change = true;
                        cnt++;
                    }
                }
                else if (cnt == 4)
                {
                    color.g -= 0.01f;
                    if (color.g <= 0)
                    {
                        color.g = 0;
                        change = true;

                    }
                }
            }
            else if (change)
            {
                if (cnt == 1)
                {
                    color.g += 0.01f;
                    if (color.g >= 1)
                    {
                        color.g = 1;
                        change = false;
                        cnt++;
                    }
                }
                else if (cnt == 3)
                {
                    color.r += 0.01f;
                    if (color.r >= 1)
                    {
                        color.r = 1;
                        change = false;
                        cnt++;
                    }
                }
            }

            smash.color = color;
            smash.ChangedColor();

            if (cnt == 5)
            {
                check = false;
                Destroy(this);
            }
            
            
        }
    }

}
