﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class OpenAudio : MonoBehaviour
{
    [SerializeField]AudioSource audio;
    [SerializeField]float OpenStartAngle = 0.01f;
    bool oneTime = false;
    bool Open = false;
    int cnt = 0;
    Quaternion temp;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        temp = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (temp != transform.rotation)
        {
            float y = transform.rotation.y - temp.y;
            cnt++;
            if (cnt > 0 && y > OpenStartAngle)
            {
                Open = true;
            }
            
        }
        if (Open && !oneTime)
        {
            audio.Play();
            oneTime = true;
        }
    }
}
