﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VRTK;

public class NoteBoard : MonoBehaviour
{
    [SerializeField] GameObject printKey;
    VRTK_InteractableObject interact;
    TextMeshPro print;
    TextMeshPro key;
    int count = 0;
    // Start is called before the first frame update
    void Start()
    {
        interact = GetComponent<VRTK_InteractableObject>();
        print = printKey.GetComponent<TextMeshPro>();
        key = GetComponentInChildren<TextMeshPro>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        if (interact.IsGrabbed())
        {
            print.text = key.text;
        }
    }
}
