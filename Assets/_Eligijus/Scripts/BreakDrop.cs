﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using VRTK;
public class BreakDrop : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    GameObject Replace;
    MeshRenderer renderer;
    Rigidbody rg;
    Vector3 currVel = Vector3.zero;
    Vector3 prevPos =  Vector3.zero;
    VRTK_InteractableObject interact;
    void Start()
    {
        rg = GetComponent<Rigidbody>();
        interact = GetComponent<VRTK_InteractableObject>();
        renderer = gameObject.GetComponent<MeshRenderer>();
        StartCoroutine(CalcVelocity());
    }

    IEnumerator CalcVelocity()
    {
        while (Application.isPlaying)
        {
            // Position at frame start
            prevPos = gameObject.transform.position;
            // Wait till it the end of the frame
            yield return new WaitForEndOfFrame();
            // Calculate velocity: Velocity = DeltaPosition / DeltaTime
            currVel = (prevPos - gameObject.transform.position) / Time.deltaTime;
            Debug.Log(currVel);
        }
    }
    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(gameObject.transform.position, transform.up*-1, Color.red, -100);
        if ((Math.Abs(currVel.x) > 1 || Math.Abs(currVel.y) > 1 || Math.Abs(currVel.z) > 1) && gameObject.activeSelf && !interact.IsGrabbed())
        {
            if (raycast(gameObject.transform.position, renderer.bounds.size.x, transform.TransformDirection(transform.right)))
            {
                Replace.SetActive(true);
                foreach (Transform child in Replace.transform)
                {
                    child.gameObject.GetComponent<Rigidbody>().velocity = rg.velocity;
                    Material[] mats = child.GetComponent<MeshRenderer>().materials;
                    mats[0] = renderer.materials[0];
                    child.GetComponent<MeshRenderer>().materials = mats;
                }
                Replace.transform.position = transform.position;
               // Replace.transform.rotation = transform.rotation;
                gameObject.SetActive(false);
                
            }
            //if (raycast(gameObject.transform.position, renderer.bounds.size.x, transform.right*-1))
            //{
            //    Replace.SetActive(true);
            //    foreach (Transform child in Replace.transform)
            //    {
            //        child.gameObject.GetComponent<Rigidbody>().velocity = rg.velocity;
            //        Material[] mats = child.GetComponent<MeshRenderer>().materials;
            //        mats[0] = renderer.materials[0];
            //        child.GetComponent<MeshRenderer>().materials = mats;
            //    }
            //    Replace.transform.position = transform.position;
            //    //Replace.transform.rotation = transform.rotation;
            //    gameObject.SetActive(false);
                
            //}
            else if (raycast(gameObject.transform.position, renderer.bounds.size.y, transform.TransformDirection(transform.up)))

            {
                Replace.SetActive(true);
                foreach (Transform child in Replace.transform)
                {
                    child.gameObject.GetComponent<Rigidbody>().velocity = rg.velocity;
                    Material[] mats = child.GetComponent<MeshRenderer>().materials;
                    mats[0] = renderer.materials[0];
                    child.GetComponent<MeshRenderer>().materials = mats;
                }
                Replace.transform.position = transform.position;
                //Replace.transform.rotation = transform.rotation;
                gameObject.SetActive(false);
                
            }
            //if (raycast(gameObject.transform.position, renderer.bounds.size.y, transform.up*-1))
            //{
            //    Replace.SetActive(true);
            //    foreach (Transform child in Replace.transform)
            //    {
            //        child.gameObject.GetComponent<Rigidbody>().velocity = rg.velocity;
            //        Material[] mats = child.GetComponent<MeshRenderer>().materials;
            //        mats[0] = renderer.materials[0];
            //        child.GetComponent<MeshRenderer>().materials = mats;
            //    }
            //    Replace.transform.position = transform.position;
            //   // Replace.transform.rotation = transform.rotation;
            //    gameObject.SetActive(false);
                
            //}
            else if (raycast(gameObject.transform.position, renderer.bounds.size.z, transform.TransformDirection(transform.forward)))
            {
                Replace.SetActive(true);
                foreach (Transform child in Replace.transform)
                {
                    child.gameObject.GetComponent<Rigidbody>().velocity = rg.velocity;
                    Material[] mats = child.GetComponent<MeshRenderer>().materials;
                    mats[0] = renderer.materials[0];
                    child.GetComponent<MeshRenderer>().materials = mats;
                }
                Replace.transform.position = transform.position;
                //Replace.transform.rotation = transform.rotation;
                gameObject.SetActive(false);
                
            }
            //if (raycast(gameObject.transform.position, renderer.bounds.size.z, transform.forward*-1))
            //{
            //    Replace.SetActive(true);
            //    foreach (Transform child in Replace.transform)
            //    {
            //        child.gameObject.GetComponent<Rigidbody>().velocity = rg.velocity;
            //        Material[] mats = child.GetComponent<MeshRenderer>().materials;
            //        mats[0] = renderer.materials[0];
            //        child.GetComponent<MeshRenderer>().materials = mats;
            //    }
            //    Replace.transform.position = transform.position;
            //    //Replace.transform.rotation = transform.rotation;
            //    gameObject.SetActive(false);
                
            //}
        }
        
    }

    bool raycast(Vector3 position , float size, Vector3 point)
    {
        //Debug.DrawRay(new Vector3(x, y, z),Vector3.down);
        RaycastHit hit;
        float RayCastRange = 2f;
        Ray ray = new Ray(position, point);
        // not harmul error
        if (Physics.Raycast(ray, out hit, RayCastRange))
        {
            if (hit.collider.tag != "GroundTag")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }
}
