﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadSceneFirst : MonoBehaviour
{
    [SerializeField] int sceneID;
    // Start is called before the first frame update
    void Start()
    {
        SceneManager.LoadSceneAsync(sceneID, LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
