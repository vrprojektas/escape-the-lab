﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColor : MonoBehaviour
{
    [ColorUsageAttribute(true, true, 0f, 8f, 0.125f, 3f)]
    [SerializeField] Color[] colors;
    [SerializeField] LiquidColor liquid;
    BottleSmash bottle;
    int index = 0;
    bool oneTime = false;
    // Start is called before the first frame update
    void Start()
    {
        if (colors.Length > 0)
        {
            bottle = GetComponent<BottleSmash>();
            index = Random.Range(0, colors.Length);
            bottle.color = colors[index];
        }
    }

    void Update()
    {
        if (!oneTime && colors.Length > 0)
        {
            bottle.ChangedColor();
            liquid.Unify();
            Destroy(this);
        }
    }
}
