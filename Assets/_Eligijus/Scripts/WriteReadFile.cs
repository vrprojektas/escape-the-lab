﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;

public class WriteReadFile
{
    string FileToRead;
    public WriteReadFile(string file)
    {
        FileToRead = file;
        if (!File.Exists(Application.persistentDataPath + "/" + FileToRead))
        {
            FileStream fs = new FileStream(Application.persistentDataPath + "/" + FileToRead, FileMode.OpenOrCreate);
            fs.Close();
        }
    }
    public void WriteData(string line)
    {
        List<Data> listData = ReadAllData();
        listData.Add(Data.Parse(line));
        listData.Sort();
        if (listData.Count >= 10)
        {
            listData = listData.GetRange(0, 10);
        }
        using (StreamWriter write = new StreamWriter(Application.persistentDataPath + "/" + FileToRead))
        {
            foreach(Data data in listData)
            {
                write.WriteLine(data.ToString());
            }
        }
        //Debug.Log(Application.persistentDataPath + "/" + FileToRead);
    }
    public List<Data> ReadAllData()
    {
        List<Data> listData = new List<Data>();
        string[] tempData = File.ReadAllLines(Application.persistentDataPath + "/" + FileToRead);
        for (int i = 0; i < tempData.Length; i++)
        {
            listData.Add(Data.Parse(tempData[i]));
        }
        listData.Sort();
        if (listData.Count >= 10)
        {
            listData = listData.GetRange(0, 10);
        }
        return listData;
    }
}
