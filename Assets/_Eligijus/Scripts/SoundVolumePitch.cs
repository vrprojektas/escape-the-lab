﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundVolumePitch : MonoBehaviour
{
    SoundControll soundControll;

    [System.Serializable]
    public class VolumeControll
    {
        
        public string tag;
        [Range(0, 3)]
        public float pitch;
        [Range(0, 1)]
        public float volume;
        bool updated;
        
    }

    public VolumeControll[] volume;
    [Range(1, 0)]
    public float masterVolume;
    float tempMaster;
    [Range(1, 0)]
    public float musicVolume;
    float tempMusic;
    [Range(1, 0)]
    public float interactVolume;
    float tempInteract;
    int count = 0;

    private void Awake()
    {
        soundControll = GetComponent<SoundControll>();
        if (PlayerPrefs.HasKey("masterVolume"))
        {
            masterVolume = PlayerPrefs.GetFloat("masterVolume");
            tempMaster = masterVolume;
        }
        else
        {
            masterVolume = 1f;
            tempMaster = 1f;
            PlayerPrefs.SetFloat("masterVolume", masterVolume);
        }
        if (PlayerPrefs.HasKey("musicVolume"))
        {
            musicVolume = PlayerPrefs.GetFloat("musicVolume");
            tempMusic = musicVolume;
        }
        else
        {
            musicVolume = 1f;
            tempMusic = 1f;
            PlayerPrefs.SetFloat("musicVolume", musicVolume);
        }
        if (PlayerPrefs.HasKey("intercatVolume"))
        {
            interactVolume = PlayerPrefs.GetFloat("intercatVolume");
            tempInteract = interactVolume;
        }
        else
        {
            interactVolume = 1f;
            tempInteract = 1f;
            PlayerPrefs.SetFloat("intercatVolume", interactVolume);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (masterVolume != tempMaster)
        {
            PlayerPrefs.SetFloat("masterVolume", masterVolume);
            tempMaster = masterVolume;
        }

        if (musicVolume != tempMusic)
        {
            PlayerPrefs.SetFloat("musicVolume", musicVolume);
            tempMusic = musicVolume;
        }
        if (tempInteract != interactVolume)
        {
            PlayerPrefs.SetFloat("intercatVolume", interactVolume);
            tempInteract = interactVolume;
        }

        if (count < volume.Length && !soundControll.resetScene)
        {

            float procentage = masterVolume / 100;
            Sound temp = soundControll.ReturnSound(volume[count].tag);
            float tempVolume = procentage * 100 * volume[count].volume;
            if (volume[count].tag != "music")
            {
                float procentageInteract = interactVolume / 100;
                tempVolume = tempVolume * 100 * procentageInteract;
                if (temp.volume != tempVolume || temp.pitch != volume[count].pitch)
                {
                    soundControll.UpdateSound(tempVolume, volume[count].pitch, volume[count].tag);
                }
            }
            else if (volume[count].tag == "music")
            {
                float procentageMusic = musicVolume / 100;
                tempVolume = tempVolume * 100 * procentageMusic;
                if (temp.volume != tempVolume || temp.pitch != volume[count].pitch)
                {
                    soundControll.UpdateSound(tempVolume, volume[count].pitch, volume[count].tag);
                }
            }
            count++;
        }
        else if (count >= volume.Length)
        {
            count = 0;
        }
    }
}
