﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AccessDenied : MonoBehaviour
{
    AudioSource audio;
    bool started = false;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    public void StartSound()
    {
        if (started && !audio.isPlaying)
        {
            started = false;
        }
        if (!started)
        {
            audio.Play();
            started = true;
        }
        
    }
}
