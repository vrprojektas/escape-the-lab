﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRTK.Controllables;
using VRTK.Controllables.PhysicsBased;

public class StartScene : MonoBehaviour
{
    [SerializeField] VRTK_BaseControllable controllable;
    [SerializeField] bool exit = false;
    [SerializeField] int sceneId = 0;
    public GameObject loadingScreen;
    bool oneTime = false;
    GameObject menu;
    ButtonClick menuClick;
    private void Start()
    {
        menu = GameObject.FindGameObjectWithTag("Pause Menu").gameObject;
        menuClick = menu.GetComponent<ButtonClick>();
    }

    protected virtual void OnEnable()
    {
        controllable = (controllable == null ? GetComponent<VRTK_BaseControllable>() : controllable);
        controllable.MaxLimitReached += MaxLimitReached;
    }

    // Update is called once per frame
    protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        if (!exit)
        {
            //SceneManager.LoadScene(sceneId);

            StartCoroutine(LoadAsynchronously(sceneId));
        }
        else
        {
            Application.Quit();
        }
    }

    IEnumerator LoadAsynchronously (int sceneIndex)
    {
        loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            Debug.Log(progress);

            yield return null;
        }
    }

    private void Update()
    {
        if (menuClick.IsOn() && !oneTime)
        {
            gameObject.GetComponent<VRTK_PhysicsPusher>().enabled = false;
            oneTime = true;
        }
        else if (!menuClick.IsOn())
        {
            gameObject.GetComponent<VRTK_PhysicsPusher>().enabled = true;
            oneTime = false;
        }
    }
}
