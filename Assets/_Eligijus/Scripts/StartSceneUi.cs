﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class StartSceneUi : MonoBehaviour
{
    [SerializeField] int playAgain = 0;
    [SerializeField] int mainMenu = 0;
    // Start is called before the first frame update
    public void MainMenu()
    {
        SceneManager.LoadScene(mainMenu);
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(playAgain);
    }
}
