﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SoftParticlesUi : MonoBehaviour
{
    TMP_Dropdown dropdown;
    // Start is called before the first frame update
    private void OnEnable()
    {
        if (dropdown == null)
            dropdown = GetComponent<TMP_Dropdown>();
        if (QualitySettings.softParticles == false)
        {
            dropdown.value = 0;
        }
        else
            dropdown.value = 1;
    }

    public void SetSoftParticle(int index)
    {
        if (index == 0)
        {
            QualitySettings.softParticles = false;
        }
        else if (index == 1)
        {
            QualitySettings.softParticles = true;
        }
        UpdateSettings.Save = true;
    }
}
