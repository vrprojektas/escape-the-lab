﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraActivation : MonoBehaviour
{
    private int i = 0;
    RenderTexture rd;
    [SerializeField]Camera camera;
    Material mat;
    Shader sh;
    static int Id = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (i == 0)
        {
            rd = new RenderTexture(512, 512, 0, RenderTextureFormat.ARGB32);
            rd.name = "renderTexture" + Id;
            rd.Create();
           
            camera.targetTexture = rd;
            sh = gameObject.GetComponent<MeshRenderer>().material.shader;
            mat = new Material(sh);
            mat.name = "EyeMaterial" + Id;
            mat.SetFloat("_Glossiness", 0.5f);
            mat.SetTexture("_MainTex", rd);
            gameObject.GetComponent<MeshRenderer>().material = mat;
            i++;
            Id++;
        }

        if (i == 0)
        {

            //gameObject.GetComponent<Camera>().enabled = false;
            gameObject.GetComponent<Camera>().enabled = false;
            i++;
        }

    }

    void OnDrawGizmosSelected()
    {
        if (camera != null)
        {
            camera.targetTexture = null;
            camera.targetTexture = rd;
        }
    }
}
