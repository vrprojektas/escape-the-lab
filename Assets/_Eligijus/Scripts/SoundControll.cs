﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundControll:MonoBehaviour
{
    [SerializeField] string[] audioTags;    

    Hashtable soundHash;
    public bool resetScene = true;
    int i = 0;
    // Update is called once per frame
    void Update()
    {
        if (resetScene)
        {
            soundHash = new Hashtable();
            if (i < audioTags.Length)
            {
                Sound sound = new Sound();
                foreach (GameObject go in GameObject.FindGameObjectsWithTag(audioTags[i]))
                {
                    sound.pitch = go.GetComponent<AudioSource>().pitch;
                    sound.volume = go.GetComponent<AudioSource>().volume;
                    sound.sources.Add(go.GetComponent<AudioSource>());
                }
                soundHash.Add(audioTags[i], sound);
            }
            else if(i >= audioTags.Length)
            {
                resetScene = false;
            }
            
        }
    }

    public void UpdateSound(float volume, float pitch, string tag)
    {
        Sound temp = (Sound)soundHash[tag];
        temp.volume = volume;
        temp.pitch = pitch;
        foreach (AudioSource item in temp.sources)
        {
            item.volume = volume;
            item.pitch = pitch;
        }
        
    }

    public Sound ReturnSound(string tag)
    {
        Sound temp = (Sound)soundHash[tag];
        return temp;
    }
}
