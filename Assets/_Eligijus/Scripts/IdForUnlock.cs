﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdForUnlock : MonoBehaviour
{

    private int Id = 0;
    // Start is called before the first frame update
    public void SetId(int id)
    {
        Id = id;
    }
    public int GetId()
    {
        return Id;
    }
}
