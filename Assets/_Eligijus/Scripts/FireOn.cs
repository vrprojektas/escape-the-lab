﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using VRTK;
public class FireOn : MonoBehaviour
{
    public VRTK_InteractableObject linkedObject;
    public GameObject particle;
    public ParticleSystem light;
    public bool Active = false;
    public bool fireEstinguished = false;
    public float time;
    public int contTimes = 0;

    void Start()
    {
        time = particle.GetComponent<ParticleSystem>().main.duration;
    }

    void FixedUpdate()
    {

        if (linkedObject.IsUsing() && contTimes == 0 && !fireEstinguished)
        {
            var sizeOver = light.sizeOverLifetime;
            AnimationCurve min = new AnimationCurve();
            min.AddKey(0.0f, 1.0f);
            min.AddKey(1.0f, 1.0f);
            sizeOver.enabled = true;
            AnimationCurve max = new AnimationCurve();
            max.AddKey(0.0f, 0.0f);
            max.AddKey(0.086f, 0.61f);
            max.AddKey(0.203f, 1.0f);
            max.AddKey(0.797f, 1.0f);
            max.AddKey(1.0f, 1.0f);
            sizeOver.size = new ParticleSystem.MinMaxCurve(1.5f, min, max);
            Active = true;
            contTimes++;
            particle.GetComponent<ParticleSystem>().Play();
        }
        else if (linkedObject.IsUsing() && contTimes > 0 && !fireEstinguished)
        {
            var sizeOver = light.sizeOverLifetime;
            AnimationCurve min = new AnimationCurve();
            min.AddKey(0.0f, 1.0f);
            min.AddKey(1.0f, 1.0f);
            sizeOver.enabled = true;
            AnimationCurve max = new AnimationCurve();
            max.AddKey(0.0f, 1.0f);
            max.AddKey(0.086f, 1.0f);
            max.AddKey(0.203f, 1.0f);
            max.AddKey(0.797f, 1.0f);
            max.AddKey(1.0f, 1.0f);
            sizeOver.size = new ParticleSystem.MinMaxCurve(1.5f, min, max);
            particle.GetComponent<ParticleSystem>().Play();
        }
        else
        {
            var sizeOver = light.sizeOverLifetime;
            AnimationCurve min = new AnimationCurve();
            min.AddKey(0.0f, 1.0f);
            min.AddKey(1.0f, 1.0f);
            sizeOver.enabled = true;
            AnimationCurve max = new AnimationCurve();
            max.AddKey(0.0f, 1.0f);
            max.AddKey(0.086f, 1.0f);
            max.AddKey(0.203f, 1.0f);
            max.AddKey(0.797f, 1.0f);
            max.AddKey(0.911f, 0.6f);
            max.AddKey(1.0f, 0.0f);
            sizeOver.size = new ParticleSystem.MinMaxCurve(1.5f, min, max);
            Active = false;
            contTimes = 0;
        }

    }

}
