﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
public class GameData
{
    public static StringBuilder NickName { get; private set; }
    public static bool? End { get; private set; }
    public static bool? Victory { get; private set; }
    public static int Time { get; private set; }
    public static bool SetUser = false;

    public static void Cear()
    {
        NickName = null;
        End = null;
        Victory = null;
        Time = 0;
        SetUser = false;
    }

    public static void SetNickName(StringBuilder nickName)
    {
        NickName = nickName;
    }

    public static void SetTime(int time)
    {
        Time = time;
    }

    public static void SetEnd(bool end)
    {
        End = end;
    }

    public static void SetVictory(bool victory)
    {
        Victory = victory;
    }
}
