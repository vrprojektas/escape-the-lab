﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class ScoreBoard : MonoBehaviour
{
    [SerializeField] GameObject prefab;
    [SerializeField] float firstXCoordinate;
    [SerializeField] float firstYCoordinate;
    [SerializeField] float firstZCoordinate;
    [SerializeField] float offSet;
    float tempPosition;
    List<Data> allData;
    WriteReadFile file;
    bool oneTime = true;
    // Start is called before the first frame update
    void Start()
    {
        tempPosition = 0;
        allData = new List<Data>();
        file = new WriteReadFile("save.data");
        allData = file.ReadAllData();
        for (int i = 0; i < allData.Count; i++)
        {
            if (i == 0)
            {
                GameObject List = Instantiate(prefab);
                List.transform.SetParent(transform);
                List.GetComponent<RectTransform>().localPosition = new Vector3(firstXCoordinate, firstYCoordinate, firstZCoordinate);
                List.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                List.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                tempPosition = firstYCoordinate;
                List.transform.Find("Place").GetComponent<TextMeshProUGUI>().text = (i+1).ToString();
                List.transform.Find("Date").GetComponent<TextMeshProUGUI>().text = allData[i].DatePlay.ToString("yyyy/MM/dd");
                List.transform.Find("User").GetComponent<TextMeshProUGUI>().text = allData[i].NickName;
                List.transform.Find("Score").GetComponent<TextMeshProUGUI>().text = allData[i].Score.ToString();
            }
            if (i > 0)
            {
                GameObject List = Instantiate(prefab);
                List.transform.SetParent(transform);
                float yPosition = tempPosition - offSet;
                List.GetComponent<RectTransform>().localPosition = new Vector3(firstXCoordinate, yPosition, firstZCoordinate);
                List.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                List.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                tempPosition = yPosition;
                List.transform.Find("Place").GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();
                List.transform.Find("Date").GetComponent<TextMeshProUGUI>().text = allData[i].DatePlay.ToString("yyyy/MM/dd");
                List.transform.Find("User").GetComponent<TextMeshProUGUI>().text = allData[i].NickName;
                List.transform.Find("Score").GetComponent<TextMeshProUGUI>().text = allData[i].Score.ToString();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameData.End == true && oneTime && GameEnd.Inserted)
        {
            DestroyAllChild();
            FillScoreBoard();
            oneTime = false;
        }
    }

    void DestroyAllChild()
    {

        foreach (Transform objects in transform)
        {
            Destroy(objects.gameObject);
        }

    }

    void FillScoreBoard()
    {
        allData = file.ReadAllData();
        for (int i = 0; i < allData.Count; i++)
        {
            if (i == 0)
            {
                GameObject List = Instantiate(prefab);
                List.transform.SetParent(transform);
                List.GetComponent<RectTransform>().localPosition = new Vector3(firstXCoordinate, firstYCoordinate, firstZCoordinate);
                List.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                List.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                tempPosition = firstYCoordinate;
                List.transform.Find("Place").GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();
                List.transform.Find("Date").GetComponent<TextMeshProUGUI>().text = allData[i].DatePlay.ToString("yyyy/MM/dd");
                List.transform.Find("User").GetComponent<TextMeshProUGUI>().text = allData[i].NickName;
                List.transform.Find("Score").GetComponent<TextMeshProUGUI>().text = allData[i].Score.ToString();
            }
            if (i > 0)
            {
                GameObject List = Instantiate(prefab);
                List.transform.SetParent(transform);
                float yPosition = tempPosition - offSet;
                List.GetComponent<RectTransform>().localPosition = new Vector3(firstXCoordinate, yPosition, firstZCoordinate);
                List.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                List.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                tempPosition = yPosition;
                List.transform.Find("Place").GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();
                List.transform.Find("Date").GetComponent<TextMeshProUGUI>().text = allData[i].DatePlay.ToString("yyyy/MM/dd");
                List.transform.Find("User").GetComponent<TextMeshProUGUI>().text = allData[i].NickName;
                List.transform.Find("Score").GetComponent<TextMeshProUGUI>().text = allData[i].Score.ToString();
            }
        }
    }
}
