﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShadowDistanceUi : MonoBehaviour
{
    Slider slider;
    private void OnEnable()
    {
        if (slider == null)
        {
            slider = GetComponent<Slider>();
        }
        slider.value = QualitySettings.shadowDistance;
    }

    public void SetShadowDistance(float index)
    {
        QualitySettings.shadowDistance = slider.value;
        UpdateSettings.Save = true;
    }
}
