﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShadowQualityResUi : MonoBehaviour
{
    TMP_Dropdown dropdown;
    // Start is called before the first frame update
    private void OnEnable()
    {
        if (dropdown == null)
        {
            dropdown = GetComponent<TMP_Dropdown>();
        }
        if (QualitySettings.shadowResolution == ShadowResolution.Low)
        {
            dropdown.value = 0;
        }
        else if (QualitySettings.shadowResolution == ShadowResolution.Medium)
        {
            dropdown.value = 1;
        }
        else if (QualitySettings.shadowResolution == ShadowResolution.High)
        {
            dropdown.value = 2;
        }
        else if (QualitySettings.shadowResolution == ShadowResolution.VeryHigh)
        {
            dropdown.value = 3;
        }

    }

    public void ChangeShadowRes(int index)
    {
        if (index == 0)
        {
            QualitySettings.shadowResolution = ShadowResolution.Low;
        }
        else if (index == 1)
        {
            QualitySettings.shadowResolution = ShadowResolution.Medium;
        }
        else if (index == 2)
        {
            QualitySettings.shadowResolution = ShadowResolution.High;
        }
        else if (index == 3)
        {
            QualitySettings.shadowResolution = ShadowResolution.VeryHigh;
        }
        UpdateSettings.Save = true;
    }

}
