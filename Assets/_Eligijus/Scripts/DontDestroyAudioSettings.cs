﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DontDestroyAudioSettings : MonoBehaviour
{
    SoundControll soundControll;
    int lenght = 0;
    int cnt = 0;
    private void Awake()
    {
        cnt = 0;
        soundControll = GetComponent<SoundControll>();
        lenght = FindObjectsOfType(typeof(SoundVolumePitch)).Length;
        if (lenght >= 1)
        {
            foreach (SoundVolumePitch item in FindObjectsOfType(typeof(SoundVolumePitch)))
            {
                cnt++;
                if (cnt == 1 && lenght == 1)
                {
                    DontDestroyOnLoad(gameObject);
                }
                if (lenght == 2 && item.gameObject.scene.name != "DontDestroyOnLoad")
                {
                    Destroy(item.gameObject);
                }
            }
            
        }
        
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        soundControll.resetScene = true;
    }

}