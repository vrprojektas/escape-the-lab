﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class IgnoreTeleport : MonoBehaviour
{
    VRTK_InteractableObject interact;
    [SerializeField] int layerWhenGrabbed = 2;
    [SerializeField] int layerWhenRealised = 0;
    // Start is called before the first frame update
    void Start()
    {
        interact = GetComponent<VRTK_InteractableObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (interact.IsGrabbed())
        {
            gameObject.layer = layerWhenGrabbed;
        }
        else
        {
            gameObject.layer = layerWhenRealised;
        }
    }
}
