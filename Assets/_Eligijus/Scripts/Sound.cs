﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound 
{

        public List<AudioSource> sources;
        [Range(0, 1)]
        public float volume;
        [Range(0, 3)]
        public float pitch;

        public Sound()
        {
            sources = new List<AudioSource>();
            volume = 0;
            pitch = 0;
        }

    
}
