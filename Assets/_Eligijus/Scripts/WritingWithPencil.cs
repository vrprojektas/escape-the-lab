﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WritingWithPencil : MonoBehaviour
{
    public Camera cam;
    private RenderTexture slapMap;
    public Shader drawShader;
    private Material drawMaterial;
    private Material myMaterial;
    [Range(1, 500)]
    public float brushSize;
    [Range(0, 1)]
    public float brushStrenght;
    public LayerMask mask;
    // Start is called before the first frame update
    void Start()
    {
        drawMaterial = new Material(drawShader);
        drawMaterial.SetVector("_Color", Color.red);
        myMaterial = GetComponent<MeshRenderer>().material;
        myMaterial.SetTexture("_SecTex", slapMap = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGBFloat));
    }
    //RaycastHit hit;

    // Update is called once per frame
    void FixedUpdate()
    {
        //if(Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse1))
        //{
        //    cam = Camera.main;
        //    if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, mask))
        //    {
        //        Debug.Log(hit.collider.name);
        //        drawMaterial.SetVector("_Coordinate", new Vector4(hit.textureCoord.x, hit.textureCoord.y, 0, 0));
        //        Debug.Log(drawMaterial.GetVector("_Coordinate"));
        //        drawMaterial.SetFloat("_Strength", brushStrenght);
        //        drawMaterial.SetFloat("_Size", brushSize);
        //        RenderTexture temp = RenderTexture.GetTemporary(slapMap.width, slapMap.height, 0, RenderTextureFormat.ARGBFloat);
        //        Graphics.Blit(slapMap, temp);
        //        Graphics.Blit(temp, slapMap, drawMaterial);
        //        RenderTexture.ReleaseTemporary(temp);
        //    }
        //}
    }

    void OnCollisionEnter(Collision collision)
    {
            cam = Camera.main;
            RaycastHit hit = new RaycastHit();
            
            Debug.DrawRay(new Vector3(0.1f, 0, 0) + collision.contacts[0].point - collision.contacts[0].normal, collision.contacts[0].normal, Color.green, 100, false);
            //Physics.Raycast(new Vector3(0.1f, 0, 0) + collision.contacts[0].point - collision.contacts[0].normal, collision.contacts[0].normal, out hit, 100, mask)
        if (Physics.Raycast(new Vector3(0.1f, 0, 0) + collision.contacts[0].point - collision.contacts[0].normal, collision.contacts[0].normal, out hit, 100, mask))
        {
                Vector3 pos = cam.WorldToScreenPoint(hit.point);
                drawMaterial.SetVector("_Coordinate", new Vector4(hit.textureCoord.x, hit.textureCoord.y, 0, 0));
                Debug.Log(pos);
                drawMaterial.SetFloat("_Strength", brushStrenght);
                drawMaterial.SetFloat("_Size", brushSize);
                RenderTexture temp = RenderTexture.GetTemporary(slapMap.width, slapMap.height, 0, RenderTextureFormat.ARGBFloat);
                Graphics.Blit(slapMap, temp);
                Graphics.Blit(temp, slapMap, drawMaterial);
                RenderTexture.ReleaseTemporary(temp);
            }
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, 256, 256), slapMap, ScaleMode.ScaleToFit, false, 1);
    }
}
