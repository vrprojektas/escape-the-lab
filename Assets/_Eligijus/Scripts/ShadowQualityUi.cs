﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShadowQualityUi : MonoBehaviour
{
    TMP_Dropdown dropdown;
    private void OnEnable()
    {
        if (dropdown == null)
        {
            dropdown = GetComponent<TMP_Dropdown>();
            if (QualitySettings.shadows == ShadowQuality.HardOnly)
            {
                dropdown.value = 0;
            }
            else if (QualitySettings.shadows == ShadowQuality.All)
            {
                dropdown.value = 1;
            }
        }
        else
        {
            if (QualitySettings.shadows == ShadowQuality.HardOnly)
            {
                dropdown.value = 0;
            }
            else if (QualitySettings.shadows == ShadowQuality.All)
            {
                dropdown.value = 1;
            }
        }
    }

    public void ChangeShadowQuality(int index)
    {
        if (index == 0)
        {
            QualitySettings.shadows = ShadowQuality.HardOnly;
        }
        else if (index == 1)
        {
            QualitySettings.shadows = ShadowQuality.All;
        }
        UpdateSettings.Save = true;
    }
}
