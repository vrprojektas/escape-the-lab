﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteMove : MonoBehaviour
{
    [SerializeField] float speed = 1;
    Rigidbody rb;

    [SerializeField] MixingScript mix;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
    }

    private void Update()
    {
        if (mix.mixed)
            rb.isKinematic = false;
    }

    void OnCollisionExit(Collision other)
    {
        rb.AddForce(transform.up * speed);
        
    }
}
