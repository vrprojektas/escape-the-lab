﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melting : MonoBehaviour
{

    [SerializeField] float melting = 0.1f;
    [SerializeField] string MeltTag;
    public ParticleSystem pscheck;
    public bool OneTimeTrigger = false;
    public bool collided = false;
    public float liquidlvl = 0.000385376f;
    public LiquidVolumeAnimator lva;
    public BottleSmash smash;

    void OnParticleCollision(GameObject other)
    {

        if (other.tag == MeltTag && !OneTimeTrigger)
        {
            if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z > 0.0005)
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z > 0.0005) // x < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z > 0.0005) // y < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z < 0.0005) // z < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
            }

            // Two checks

            else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z > 0.0005) // x y < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z < 0.0005) // x z < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
            }
            else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z < 0.0005) // y z < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z);
            }

            if (this.transform.localScale.z < 0.0005 && this.transform.localScale.x < 0.0005 && this.transform.localScale.y < 0.0005)
            {
                //Destroy(gameObject);
            }

        }
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.collider.tag == MeltTag && OneTimeTrigger)
        {
            if(collision.gameObject.GetComponent<DryIceCheck>() != null && lva == null){
                lva = collision.gameObject.GetComponent<DryIceCheck>().lva;
            }
            if (lva != null && gameObject.transform.position.y < lva.finalPoint.y)
            {
                if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z > 0.0005)
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z > 0.0005) // x < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z > 0.0005) // y < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z < 0.0005) // z < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
                }

                // Two checks

                else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z > 0.0005) // x y < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z < 0.0005) // x z < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
                }
                else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z < 0.0005) // y z < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z);
                }

                if (this.transform.localScale.z < 0.0005 && this.transform.localScale.x < 0.0005 && this.transform.localScale.y < 0.0005)
                {
                    Destroy(gameObject);
                }
            }
            else if(lva == null)
            {

                if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z > 0.0005)
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z > 0.0005) // x < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z > 0.0005) // y < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z < 0.0005) // z < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
                }

                // Two checks

                else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z > 0.0005) // x y < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z < 0.0005) // x z < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
                }
                else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z < 0.0005) // y z < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z);
                }

                if (this.transform.localScale.z < 0.0005 && this.transform.localScale.x < 0.0005 && this.transform.localScale.y < 0.0005)
                {
                    Destroy(gameObject);
                }
            }
            gameObject.GetComponent<Collider>().isTrigger = true;
            collided = true;
        }
    }

    private void Update()
    {
        if (collided && smash != null && smash.broken)
        {
            pscheck.Stop(true);
            collided = false;

        }
        if (OneTimeTrigger && collided && gameObject.transform.position.y < lva.finalPoint.y)
        {
            if(pscheck != null && !pscheck.isPlaying)
            {
                pscheck.Play(true);
            }
            if (lva.level < liquidlvl)
            {
                pscheck.Stop(true);
            }
                if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z > 0.0005)
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z > 0.0005) // x < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z > 0.0005) // y < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z < 0.0005) // z < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
                }

                // Two checks

                else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z > 0.0005) // x y < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z - melting);
                }
                else if (this.transform.localScale.x < 0.0005 && this.transform.localScale.y > 0.0005 && this.transform.localScale.z < 0.0005) // x z < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
                }
                else if (this.transform.localScale.x > 0.0005 && this.transform.localScale.y < 0.0005 && this.transform.localScale.z < 0.0005) // y z < 0.05
                {
                    this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z);
                }

                if (this.transform.localScale.z < 0.0005 && this.transform.localScale.x < 0.0005 && this.transform.localScale.y < 0.0005)
                {
                    Destroy(gameObject);
                }
            
        }
    }

}
