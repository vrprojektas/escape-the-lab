﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetSettingsUi : MonoBehaviour
{

    void Start()
    {
        if (!PlayerPrefs.HasKey("Count"))
        {
            PlayerPrefs.SetInt("Count", 0);

            // set backup shadow Quality
            PlayerPrefs.SetInt("LowShadowQuality", 0);
            PlayerPrefs.SetInt("MediumShadowQuality", 0);
            PlayerPrefs.SetInt("HighShadowQuality", 1);
            PlayerPrefs.SetInt("VeryHighShadowQuality", 2);

            // set backup shadows
            PlayerPrefs.SetInt("LowShadows", 0);
            PlayerPrefs.SetInt("MediumShadows", 1);
            PlayerPrefs.SetInt("HighShadows", 2);
            PlayerPrefs.SetInt("VeryHighShadows", 2);

            // set backup shadow Cascades

            PlayerPrefs.SetInt("LowShadowCascades", 0);
            PlayerPrefs.SetInt("MediumShadowsCascades", 0);
            PlayerPrefs.SetInt("HighShadowsCascades", 1);
            PlayerPrefs.SetInt("VeryHighShadowsCascades", 2);

            // set backup Anti Aliasing
            PlayerPrefs.SetInt("LowAntiAliasing", 0);
            PlayerPrefs.SetInt("MediumAntiAliasing", 0);
            PlayerPrefs.SetInt("HighAntiAliasing", 2);
            PlayerPrefs.SetInt("VeryHighAntiAliasing", 4);

            // set backup Shadow Distance
            PlayerPrefs.SetInt("LowShadowDistance", 10);
            PlayerPrefs.SetInt("MediumShadowDistance", 20);
            PlayerPrefs.SetInt("HighShadowDistance", 40);
            PlayerPrefs.SetInt("VeryHighShadowDistance", 70);

            // set backup soft particles
            PlayerPrefs.SetInt("LowSoftParticles", 0);
            PlayerPrefs.SetInt("MediumSoftParticles", 0);
            PlayerPrefs.SetInt("HighSoftParticles", 1);
            PlayerPrefs.SetInt("VeryHighSoftParticles", 1);
        }

        
    }

    public void Reset()
    {
        for (int i = 0; i < 4; i++)
        {
            QualitySettings.SetQualityLevel(i, true);
            if (i == 0)
            {

                // shadow resolution
                if (PlayerPrefs.GetInt("LowShadowQuality") == 0)
                {
                    QualitySettings.shadowResolution = ShadowResolution.Low;
                }
                else if (PlayerPrefs.GetInt("LowShadowQuality") == 1)
                {
                    QualitySettings.shadowResolution = ShadowResolution.Medium;
                }
                else if (PlayerPrefs.GetInt("LowShadowQuality") == 2)
                {
                    QualitySettings.shadowResolution = ShadowResolution.High;
                }
                else if (PlayerPrefs.GetInt("LowShadowQuality") == 3)
                {
                    QualitySettings.shadowResolution = ShadowResolution.VeryHigh;
                }

                // shadow
                if (PlayerPrefs.GetInt("LowShadows") == 0)
                {
                    QualitySettings.shadows = ShadowQuality.Disable;
                }
                else if (PlayerPrefs.GetInt("LowShadows") == 1)
                {
                    QualitySettings.shadows = ShadowQuality.HardOnly;
                }
                else if (PlayerPrefs.GetInt("LowShadows") == 2)
                {
                    QualitySettings.shadows = ShadowQuality.All;
                }

                // shadow cascade

                if (PlayerPrefs.GetInt("LowShadowCascades") == 0)
                {
                    QualitySettings.shadowCascades = 0;
                }
                else if (PlayerPrefs.GetInt("LowShadowCascades") == 1)
                {
                    QualitySettings.shadowCascades = 2;
                }
                else if (PlayerPrefs.GetInt("LowShadowCascades") == 2)
                {
                    QualitySettings.shadowCascades = 4;
                }


                // shadow distance

                QualitySettings.shadowDistance = PlayerPrefs.GetInt("LowShadowDistance");

                // anti-aliasing

                QualitySettings.antiAliasing = PlayerPrefs.GetInt("LowAntiAliasing");

                // texture resolution

                if (PlayerPrefs.GetInt("LowSoftParticles") == 0)
                {
                    QualitySettings.softParticles = false;
                }
                else if (PlayerPrefs.GetInt("LowSoftParticles") == 1)
                {
                    QualitySettings.softParticles = true;
                }
            }

            else if (i == 1)
            {

                // shadow resolution
                if (PlayerPrefs.GetInt("MediumShadowQuality") == 0)
                {
                    QualitySettings.shadowResolution = ShadowResolution.Low;
                }
                else if (PlayerPrefs.GetInt("MediumShadowQuality") == 1)
                {
                    QualitySettings.shadowResolution = ShadowResolution.Medium;
                }
                else if (PlayerPrefs.GetInt("MediumShadowQuality") == 2)
                {
                    QualitySettings.shadowResolution = ShadowResolution.High;
                }
                else if (PlayerPrefs.GetInt("MediumShadowQuality") == 3)
                {
                    QualitySettings.shadowResolution = ShadowResolution.VeryHigh;
                }

                if (PlayerPrefs.GetInt("MediumShadows") == 0)
                {
                    QualitySettings.shadows = ShadowQuality.Disable;
                }
                else if (PlayerPrefs.GetInt("MediumShadows") == 1)
                {
                    QualitySettings.shadows = ShadowQuality.HardOnly;
                }
                else if (PlayerPrefs.GetInt("MediumShadows") == 2)
                {
                    QualitySettings.shadows = ShadowQuality.All;
                }

                // shadow cascade

                if (PlayerPrefs.GetInt("MediumShadowCascades") == 0)
                {
                    QualitySettings.shadowCascades = 0;
                }
                else if (PlayerPrefs.GetInt("MediumShadowCascades") == 1)
                {
                    QualitySettings.shadowCascades = 2;
                }
                else if (PlayerPrefs.GetInt("MediumShadowCascades") == 2)
                {
                    QualitySettings.shadowCascades = 4;
                }


                // shadow distance

                QualitySettings.shadowDistance = PlayerPrefs.GetInt("MediumShadowDistance");

                // anti-aliasing

                QualitySettings.antiAliasing = PlayerPrefs.GetInt("MediumAntiAliasing");

                // texture resolution

                if (PlayerPrefs.GetInt("MediumSoftParticles") == 0)
                {
                    QualitySettings.softParticles = false;
                }
                else if (PlayerPrefs.GetInt("MediumSoftParticles") == 1)
                {
                    QualitySettings.softParticles = true;
                }
            }

            else if(i == 2)
            { 
                if (PlayerPrefs.GetInt("HighShadows") == 0)
                {
                    QualitySettings.shadows = ShadowQuality.Disable;
                }
                else if (PlayerPrefs.GetInt("HighShadows") == 1)
                {
                    QualitySettings.shadows = ShadowQuality.HardOnly;
                }
                else if (PlayerPrefs.GetInt("HighShadows") == 2)
                {
                    QualitySettings.shadows = ShadowQuality.All;
                }

                // shadow cascade

                if (PlayerPrefs.GetInt("HighShadowCascades") == 0)
                {
                    QualitySettings.shadowCascades = 0;
                }
                else if (PlayerPrefs.GetInt("HighShadowCascades") == 1)
                {
                    QualitySettings.shadowCascades = 2;
                }
                else if (PlayerPrefs.GetInt("HighShadowCascades") == 2)
                {
                    QualitySettings.shadowCascades = 4;
                }


                // shadow distance

                QualitySettings.shadowDistance = PlayerPrefs.GetInt("HighShadowDistance");

                // anti-aliasing

                QualitySettings.antiAliasing = PlayerPrefs.GetInt("HighAntiAliasing");

                // texture resolution

                if (PlayerPrefs.GetInt("HighSoftParticles") == 0)
                {
                    QualitySettings.softParticles = false;
                }
                else if (PlayerPrefs.GetInt("HighSoftParticles") == 1)
                {
                    QualitySettings.softParticles = true;
                }
            }

            else if (i == 3)
            {
                if (PlayerPrefs.GetInt("VeryHighShadows") == 0)
                {
                    QualitySettings.shadows = ShadowQuality.Disable;
                }
                else if (PlayerPrefs.GetInt("VeryHighShadows") == 1)
                {
                    QualitySettings.shadows = ShadowQuality.HardOnly;
                }
                else if (PlayerPrefs.GetInt("VeryHighShadows") == 2)
                {
                    QualitySettings.shadows = ShadowQuality.All;
                }

                // shadow cascade

                if (PlayerPrefs.GetInt("VeryHighShadowCascades") == 0)
                {
                    QualitySettings.shadowCascades = 0;
                }
                else if (PlayerPrefs.GetInt("VeryHighShadowCascades") == 1)
                {
                    QualitySettings.shadowCascades = 2;
                }
                else if (PlayerPrefs.GetInt("VeryHighShadowCascades") == 2)
                {
                    QualitySettings.shadowCascades = 4;
                }


                // shadow distance

                QualitySettings.shadowDistance = PlayerPrefs.GetInt("VeryHighShadowDistance");

                // anti-aliasing

                QualitySettings.antiAliasing = PlayerPrefs.GetInt("VeryHighAntiAliasing");

                // texture resolution

                if (PlayerPrefs.GetInt("VeryHighSoftParticles") == 0)
                {
                    QualitySettings.softParticles = false;
                }
                else if (PlayerPrefs.GetInt("VeryHighSoftParticles") == 1)
                {
                    QualitySettings.softParticles = true;
                }
            }
            
        }
        UpdateSettings.Save = true;
        transform.parent.gameObject.SetActive(false);
        transform.parent.gameObject.SetActive(true);
    }

}
