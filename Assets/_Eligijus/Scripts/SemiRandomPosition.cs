﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SemiRandomPosition : MonoBehaviour
{
    [SerializeField] GameObject[] points;
    [SerializeField] Vector3 offset;
    Transform parent;
    // Start is called before the first frame update
    void Start()
    {
        int index = Random.Range(0, points.Length);
        parent = gameObject.transform.parent;
        Vector3 position = new Vector3(points[index].transform.position.x - offset.x, points[index].transform.position.y-offset.y, points[index].transform.position.z-offset.z);
        parent.position = position;     
    }

}
