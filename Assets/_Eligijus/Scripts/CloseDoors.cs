﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.Controllables.PhysicsBased;

public class CloseDoors : MonoBehaviour
{
    [SerializeField] GameObject doorController;
    [SerializeField] float doorSpeed = 0.05f;
    [SerializeField] GroundControll controll;
    Rigidbody rb;
    public bool isClosing = false;
    VRTK_PhysicsRotator rotator;
    int count = 0;
    // Start is called before the first frame update
    void Start()
    {
        rotator = doorController.GetComponent<VRTK_PhysicsRotator>();
        rb = doorController.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (doorController.transform.rotation.eulerAngles.y < 0.002 && doorController.transform.rotation.eulerAngles.y > 0 && rotator.isLocked)
        {
            rotator.enabled = false;
            Destroy(this);
            count++;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "[VRTK][AUTOGEN][Controller][NearTouch][CollidersContainer]" && count < 1 && doorController.transform.rotation.eulerAngles.y > 1)
        {
            isClosing = true;
            ForceToClose();
            Lock();
            count++;
        }
        else if (other.name == "[VRTK][AUTOGEN][Controller][NearTouch][CollidersContainer]" && count < 1 && doorController.transform.rotation.eulerAngles.y < 0.002)
        {
            isClosing = true;
            Lock();
            count++;
        }
    }

    void ForceToClose()
    {
        rb.AddForce(transform.right * doorSpeed);
    }

    void Lock()
    {
        controll.coridorOff();
        rotator.isLocked = true;
    }
}
