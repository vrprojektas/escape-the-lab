﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.VR;
public class StartResolutionUi : MonoBehaviour
{
    TMP_Dropdown dropdown;
    int indexResolution = 0;
    bool found = false;
    List<Resolution> res;
    public static bool fullScreen = false;
    public bool firstTime = true;

    // Start is called before the first frame update
    void OnEnable()
    {
        //reikia padaryti tik pirma karta kaip pajungi game naudoja screen.current, o poto saved naudoti, ir kai atidaro game tik tada sukuria lista, ir poto jo nebekuria is naujo

        indexResolution = 0;
        if (firstTime)
        {
            found = false;
            if (PlayerPrefs.HasKey("WindowMode"))
            {
                if (PlayerPrefs.GetInt("WindowMode") == 0)
                {
                    fullScreen = true;
                }
                else
                {
                    fullScreen = false;
                }
            }
            else
            {
                fullScreen = Screen.fullScreen;
            }
            Resolution[] resolutions = Screen.resolutions;
            dropdown = GetComponent<TMP_Dropdown>();
            dropdown.ClearOptions();
            List<string> options = new List<string>();
            res = new List<Resolution>();
            
            foreach (Resolution item in resolutions)
            {
                res.Add(item);
                options.Add(item.width + "X" + item.height);
                if (!PlayerPrefs.HasKey("ResolutionSave"))
                {
                    if (item.width != Screen.width && item.height != Screen.height && !found)
                    {
                        indexResolution++;
                    }
                    else if (item.width == Screen.width && item.height == Screen.height && !found)
                    {
                        found = true;
                    }
                }
                else
                {
                    if (item.width + "X" + item.height != PlayerPrefs.GetString("ResolutionSave") && !found)
                    {
                        indexResolution++;
                    }
                    else if (item.width + "X" + item.height == PlayerPrefs.GetString("ResolutionSave") && !found)
                    {
                        found = true;
                    }
                }


            }
            dropdown.AddOptions(options);
            firstTime = false;
        }
        else
        {
            found = false;
            for (int i = 0; i < res.Count; i++)
            {
                if (!PlayerPrefs.HasKey("ResolutionSave"))
                {
                    if (res[i].width == Screen.width && res[i].height == Screen.height && !found)
                    {
                        found = true;
                        indexResolution = i;
                    }
                }
                else
                {
                    if (res[i].width + "X" + res[i].height == PlayerPrefs.GetString("ResolutionSave") && !found)
                    {
                        found = true;
                        indexResolution = i;
                    }
                }
            }
        }
        dropdown.value = indexResolution;
        ChangeResolution(indexResolution);


    }

    public void ChangeResolution(int index)
    {
        Screen.SetResolution(res[index].width, res[index].height, fullScreen);
        PlayerPrefs.SetString("ResolutionSave", res[index].width + "X" + res[index].height);
        //UpdateSettings.Save = true;
    }


}
