﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SliderDefaultValue : MonoBehaviour
{
    [SerializeField] bool effects;
    [SerializeField] bool music;
    [SerializeField] bool master;
    Slider slider;

    void OnEnable()
    {
        slider = GetComponent<Slider>();
        if (master)
        {
            if (PlayerPrefs.HasKey("masterVolume"))
            {
                slider.value = PlayerPrefs.GetFloat("masterVolume");
            }
            else
            {
                slider.value = 1;
            }
        }
        else if (music)
        {
            if (PlayerPrefs.HasKey("musicVolume"))
            {
                slider.value = PlayerPrefs.GetFloat("musicVolume");
            }
            else
            {
                slider.value = 1;
            }
        }
        else if (effects)
        {
            if (PlayerPrefs.HasKey("intercatVolume"))
            {
                slider.value = PlayerPrefs.GetFloat("intercatVolume");
            }
            else
            {
                slider.value = 1;
            }
        }
    }
}
