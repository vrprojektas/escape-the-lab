﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Data : IComparable<Data>
{
    public int Place { get; private set; }
    public DateTime DatePlay { get; private set; }
    public string NickName { get; private set; }
    public float Score { get; private set; }

    public Data()
    {

    }

    public Data(DateTime datePlay, string nickName, float score)
    {
        DatePlay = datePlay;
        NickName = nickName;
        Score = score;
    }
    public static Data Parse(string line)
    {
        string[] data = line.Split(';');
        DateTime datePlay = DateTime.Parse(data[0]);
        string nickName = data[1];
        float score = float.Parse(data[2]);

        return new Data(datePlay, nickName, score);
    }
    public override string ToString()
    {
        return DatePlay + ";" + NickName + ";" + Score;
    }

    public int CompareTo(Data aj)
    {

        if (aj.Score == Score)
        {
            if (aj.DatePlay > DatePlay)
            {
                return 1;
            }
            if (aj.DatePlay < DatePlay)
            {
                return 0;
            }
        }
        if (aj.Score > Score)
        {
            return 1;
        }
        if (aj.Score < Score)
        {
            return -1;
        }

        // The orders are equivalent.
        return 0;
    }
}
