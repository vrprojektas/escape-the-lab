﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using UnityEngine.Events;
public class UserNameController : MonoBehaviour
{
    [SerializeField] int charLength;
    StringBuilder UserName;
    bool shift = false;
    bool set = false;

    void Start()
    {
        UserName = new StringBuilder();    
    }
    // Start is called before the first frame update
    public void AddCharacter(string a)
    {
        if (!set && UserName.Length <= charLength)
        {
            if (!shift)
            {
                UserName.Append(a.ToLower());
            }
            else
            {
                UserName.Append(a.ToUpper());
            }
        }
    }

    public void RemoveLast()
    {
        if (!set && UserName.Length > 0)
        {
            UserName.Remove(UserName.Length - 1, 1);
        }
    }

    public void Shift()
    {
        if (!shift)
        {
            shift = true;
        }
        else
        {
            shift = false;
        }
    }

    public void SetUsername()
    {
        if (!set)
        {
            GameData.SetUser = true;
            GameData.SetNickName(UserName);
            set = true;
        }
    }

    public string getUsername()
    {
        return UserName.ToString();
    }
}
