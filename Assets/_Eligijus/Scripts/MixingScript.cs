﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixingScript : MonoBehaviour
{
    [SerializeField] string mixedTag = "Mixed";
    [SerializeField] int specialLimit = 0;
    [SerializeField] string fire;
    [SerializeField] List<string> ignoreCollision;
    [SerializeField] GameObject ps;
    [SerializeField] List<string> LiquidTags;
    [SerializeField] int[] absorve;
    [SerializeField] bool special;
    List<string> tags;
    public bool mix = true;
    int[] liquidAbsorve;
    int MixSpec = 0;
    int check = 0;
    int cnt = 0;
    int indexCheckTag = 0;
    bool checkForMixed = false;
    public bool mixed = false;

    void Start()
    {
        liquidAbsorve = new int[LiquidTags.Count];
        tags = new List<string>();
    }

    void OnParticleCollision(GameObject other)
    {

        if (fire != other.tag && other.tag != "Untagged" && !ignoreCollision.Contains(other.tag) && other.tag != mixedTag)
        {
            if (!tags.Contains(other.tag))
            {
                tags.Add(other.tag);
            }
            int i = 0;
            foreach (string tag in LiquidTags)
            {
                if (other.tag == tag && absorve[i] > liquidAbsorve[i])
                {
                    liquidAbsorve[i]++;
                    break;
                }
                i++;
            }
        }
        else if (other.tag == mixedTag && special)
        {
            MixSpec++;

        }

    }
    void Update()
    {
        if (tags.Count >= LiquidTags.Count)
        {
            if (indexCheckTag < tags.Count)
            {
                checkForMixed = false;
                string tag = tags[indexCheckTag];
                if (!LiquidTags.Contains(tag))
                {
                    mix = false;
                }
                indexCheckTag++;
            }
            else if (indexCheckTag >= tags.Count)
            {
                indexCheckTag = 0;
                checkForMixed = true;
            }

            if (checkForMixed)
            {
                if (!mixed)
                {
                    if (cnt < liquidAbsorve.Length)
                    {
                        if (liquidAbsorve[cnt] >= absorve[cnt])
                        {
                            check++;
                        }
                        cnt++;
                    }
                    else
                    {
                        cnt = 0;
                        check = 0;
                    }
                    if ((check == liquidAbsorve.Length && tags.Count == LiquidTags.Count && mix) || (special && specialLimit <= MixSpec && mix))
                    {
                        mixed = true;
                        ps.tag = mixedTag;
                    }
                }
                else
                {
                    if ((check == liquidAbsorve.Length && tags.Count > LiquidTags.Count && !mix) || (!mix && special && (tags.Count < LiquidTags.Count)))
                    {
                        mixed = false;
                        ps.tag = "Untagged";
                    }
                }
            }
        }
    }

    public void ResetMix()
    {
        liquidAbsorve = new int[LiquidTags.Count];
        tags = new List<string>();
        checkForMixed = false;
        indexCheckTag = 0;
        mixed = false;
        mix = true;
        cnt = 0;
        check = 0;
        ps.tag = "Untagged";
    }

}