﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;

public class KeyForKeypad : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] VRTK_BaseControllable controllable;
    [SerializeField] LockUnlockWithPin lockController;
    ButtonSound sound;
    int count = 0;
    public int outputKey = -1;

    private void Start()
    {
        sound = GetComponent<ButtonSound>();
    }

    protected virtual void OnEnable()
    {
        controllable = (controllable == null ? GetComponent<VRTK_BaseControllable>() : controllable);

            controllable.MaxLimitReached += MaxLimitReached;
    }


    protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        if (outputKey != -1 && count < 2)
        {
            count++;
        }
        if (outputKey != -1 && count > 1)
        {
            lockController.SetNumber(outputKey);
            sound.check = true;
        }
    }



}
