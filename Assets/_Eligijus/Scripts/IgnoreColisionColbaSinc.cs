﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreColisionColbaSinc : MonoBehaviour
{
    public List<Collider> go;
    public Collider thisCollider;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < go.Count; i++)
        {
            Physics.IgnoreCollision(thisCollider, go[i], true);
        }
    }
}
