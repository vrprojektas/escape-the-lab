﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccessGranted : MonoBehaviour
{
    AudioSource source;
    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlaySound()
    {
        if(!source.isPlaying)
        {
            source.Play();
        }
    }
}
