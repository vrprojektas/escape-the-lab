﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FindCamera : MonoBehaviour
{
    Camera cam;
    bool oneTime = true;
    [SerializeField]bool changeDistance = false;
    [SerializeField] float distance = 0.35f;
    Canvas canvas;
    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        if (cam == null)
        {
            cam = Camera.main;
        }
        if (cam != null && oneTime)
        {
            canvas.worldCamera = cam;
        }
        if (changeDistance)
        {
            canvas.planeDistance = distance;
            changeDistance = false;
        }
    }
}
