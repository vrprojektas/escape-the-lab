﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSound : MonoBehaviour
{
    AudioSource audio;
    bool Fire = true;
    LightOn fireScript;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        fireScript = GetComponent<LightOn>();
    }

    // Update is called once per frame
    void Update()
    {
        if (fireScript.Started && Fire)
        {
            audio.Play();
            Fire = false;
        }
        else if (!fireScript.Started && !Fire)
        {
            Fire = true;
        }
    }
}
