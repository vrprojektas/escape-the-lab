﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables.PhysicsBased;
using VRTK;

public class LockUnlockWithKey : MonoBehaviour
{
    [SerializeField] GameObject[] Note;
    [SerializeField] GameObject doors;
    [SerializeField] string checkKey;
    [SerializeField] GameObject keySnap;
    VRTK_PhysicsRotator rotator;
    VRTK_SnapDropZone snap;
    IdForUnlock[] unlock;
    public bool Open = false;
    bool oneTime = true;
    bool check = false;
    private int unlockId = 0;
    bool rigidbodyExists = false;
    int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        rotator = doors.GetComponent<VRTK_PhysicsRotator>();
        snap = keySnap.GetComponent<VRTK_SnapDropZone>();
        rotator.enabled = false;
        foreach ( GameObject obj in Note )
        {
            obj.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (!check)
        {
            unlock = FindObjectsOfType<IdForUnlock>();
            if (index < unlock.Length)
            {
                if (unlock[index] != null && unlock[index].tag == checkKey)
                {
                    int id = Random.Range(1, 20);
                    unlockId = id;
                    unlock[index].SetId(id);
                    check = true;
                }
                else if(unlock[index] != null && unlock[index].tag != checkKey)
                {
                    index++;
                }
                
            }
            else if(index >= unlock.Length)
            {
                index = 0;
            }

        }
        else
        {
            if (Open == true && oneTime)
            {
                rotator.enabled = true;
                foreach (GameObject obj in Note)
                {
                    obj.SetActive(true);
                }
                oneTime = false;
                rigidbodyExists = true;
                Destroy(this);
            }
            if (snap.IsSnapped() && unlock[index].GetId() == unlockId)
            {
                Open = true;
            }
        }
        

    }

    public bool isChestOpen()
    {
        if (rigidbodyExists)
            return Open;
        else
            return false;
    }
}
