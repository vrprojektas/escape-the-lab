﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameEnd : MonoBehaviour
{
    bool oneTime = true;
    public static bool Inserted = false;
    // Start is called before the first frame update
    void Start()
    {
        Inserted = false;
        GameData.Cear();
    }

    // Update is called once per frame
    void Update()
    {

        if (GameData.End == true)
        {
            if (GameData.Victory == true && GameData.SetUser && !Inserted)
            {
                SaveData();
            }
        }
    }

    void SaveData()
    {
        WriteReadFile ff = new WriteReadFile("save.data");
        Data info = new Data(DateTime.Now, GameData.NickName.ToString(), GameData.Time);
        ff.WriteData(info.ToString());
        Inserted = true;
    }
}
