﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiButtonControll : MonoBehaviour
{
    [SerializeField] GameObject loadingScreen;
    ButtonClick menu;
    SoundVolumePitch volumeControll;

    private void OnEnable()
    {
        volumeControll = (SoundVolumePitch)FindObjectOfType(typeof(SoundVolumePitch));
        menu = gameObject.transform.parent.parent.GetComponent<ButtonClick>();
    }

    public void LoadLevel(int index)
    {
        StartCoroutine(LoadAsynchronously(index));
       
    }

    public void RestartLevel()
    {
        StartCoroutine(LoadAsynchronously(SceneManager.GetActiveScene().buildIndex));

    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            Debug.Log(progress);

            yield return null;
        }
    }

    public void LoadExit()
    {
        Debug.Log("EXIT");
        Application.Quit();
    }

    public void BackToPlay()
    {
        menu.backToGame = true;
        
    }

    public void MasterVolumeSlider(float masterVolume)
    {
        volumeControll.masterVolume = masterVolume;
    }

    public void FXVolumeSlider(float fxVolume)
    {
        volumeControll.interactVolume = fxVolume;
    }

    public void MusicVolumeSlider(float musicVolume)
    {
        volumeControll.musicVolume = musicVolume;
    }
}
