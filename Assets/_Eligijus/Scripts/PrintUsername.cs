﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PrintUsername : MonoBehaviour
{
    UserNameController userController;
    TextMeshPro user;
    // Start is called before the first frame update
    void Start()
    {
        userController = GetComponentInParent<UserNameController>();
        user = GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        user.text = userController.getUsername();
    }
}
