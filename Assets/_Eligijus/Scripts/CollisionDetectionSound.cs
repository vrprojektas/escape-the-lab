﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetectionSound : MonoBehaviour
{
    [SerializeField] DropSound drop;
    // Start is called before the first frame update
    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 1)
        {
            drop.PlaySound();
        }

    }


}
