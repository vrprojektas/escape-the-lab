﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundControll : MonoBehaviour
{
    [SerializeField] GameObject coridor;
    [SerializeField] GameObject finish;
    [SerializeField] GameObject finisObj;
    bool coridorCheck;
    bool finishCheck;
    // Start is called before the first frame update
    void Start()
    {
        coridorCheck = true;
        finishCheck = false;
        finish.SetActive(false);
        finisObj.SetActive(false);
    }

    public void coridorOff()
    {
        coridorCheck = false;
        coridor.SetActive(false);
    }

    public void finishOff()
    {
        finishCheck = false;
        finish.SetActive(false);
        finisObj.SetActive(false);
    }

    public void finishOn()
    {
        finishCheck = true;
        finish.SetActive(true);
        finisObj.SetActive(true);
    }

    public void coridorOn()
    {
        coridorCheck = true;
        coridor.SetActive(true);
    }

}
