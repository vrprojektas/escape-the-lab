﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAluminimum : MonoBehaviour
{
    [SerializeField] float melting = 0.001f;
    [SerializeField] MixingScript mix;
    [SerializeField] ObjectCheck aluminium;
    public bool destroy = false;
    // Update is called once per frame
    void Update()
    {
        if (mix.mixed && aluminium.added && destroy)
        {
            if (this.transform.localScale.x > 0.05 && this.transform.localScale.y > 0.05 && this.transform.localScale.z > 0.05)
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x < 0.05 && this.transform.localScale.y > 0.05 && this.transform.localScale.z > 0.05) // x < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x > 0.05 && this.transform.localScale.y < 0.05 && this.transform.localScale.z > 0.05) // y < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x > 0.05 && this.transform.localScale.y > 0.05 && this.transform.localScale.z < 0.05) // z < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
            }

            // Two checks

            else if (this.transform.localScale.x < 0.05 && this.transform.localScale.y < 0.05 && this.transform.localScale.z > 0.05) // x y < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z - melting);
            }
            else if (this.transform.localScale.x < 0.05 && this.transform.localScale.y > 0.05 && this.transform.localScale.z < 0.05) // x z < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y - melting * 10, this.transform.localScale.z);
            }
            else if (this.transform.localScale.x > 0.05 && this.transform.localScale.y < 0.05 && this.transform.localScale.z < 0.05) // y z < 0.05
            {
                this.transform.localScale = new Vector3(this.transform.localScale.x - melting * 10, this.transform.localScale.y, this.transform.localScale.z);
            }

            if (this.transform.localScale.z < 0.05 && this.transform.localScale.x < 0.05 && this.transform.localScale.y < 0.05)
            {
                Destroy(gameObject);
            }
        }
    }
}
