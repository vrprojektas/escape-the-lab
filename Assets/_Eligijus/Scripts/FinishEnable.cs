﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishEnable : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (GameData.Victory == false && GameData.End == true)
        {
            transform.Find("Canvas").gameObject.SetActive(true);
        }
    }
}
