﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCheck : MonoBehaviour
{
    [SerializeField] List<string> ignore;
    [SerializeField] string ObcectTag;
    public bool added = false;
    List<string> tags;
    private int tagsCount = 0;
    private int count = 0;
    // Start is called before the first frame update
    void Start()
    {
        tags = new List<string>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(tags.Count);
        if (tagsCount >= 1 && count < tagsCount)
        {
            if (tags[count] == ObcectTag)
            {
                added = true;
            }
            else if (tags[count] != ObcectTag) {
                added = false;
                count = tags.Count;
            }
            count++;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == ObcectTag)
        {
            Debug.Log(other.tag);
            if (!tags.Contains(ObcectTag))
            {
                tags.Add(ObcectTag);
            }
            other.gameObject.GetComponent<DestroyAluminimum>().destroy = true;
        }
        if (other.tag != ObcectTag && !ignore.Contains(other.tag))
        {
            Debug.Log(other.tag);
            if (!tags.Contains(other.tag))
            {
                tags.Add(other.tag);
            }
        }
        tagsCount = tags.Count;
    }

    public void ResetObcect()
    {
        tags = new List<string>();
        added = false;
        count = 0;
        tagsCount = 0;
    }
    
}
