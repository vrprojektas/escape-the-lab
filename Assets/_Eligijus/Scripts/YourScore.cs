﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class YourScore : MonoBehaviour
{
    bool oneTime = true;
    void Update()
    {
        if (GameData.End == true && GameData.Victory == false && oneTime)
        {
            GetComponent<TextMeshProUGUI>().text = GetComponent<TextMeshProUGUI>().text + GameData.Time;
            oneTime = false;
        }
    }
}
