﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;

public class GoFoward : MonoBehaviour
{
    [SerializeField] VRTK_BaseControllable controllableMain;
    [SerializeField] VRTK_BaseControllable controllableAudio;
    [SerializeField] VRTK_BaseControllable controllablegraphics;
    [SerializeField] GameObject main;
    [SerializeField] GameObject settings;
    [SerializeField] GameObject audioSettings;
    [SerializeField] GameObject graphics;
    [SerializeField] float seconds;
    // Start is called before the first frame update
    protected virtual void OnEnable()
    {
        controllableMain.MaxLimitReached += MaxLimitReachedMain;
        controllableAudio.MaxLimitReached += MaxLimitReachedAudio;
        //controllablegraphics.MaxLimitReached += MaxLimitReachedGraphics;
    }

    protected virtual void MaxLimitReachedMain(object sender, ControllableEventArgs e)
    {
        StartCoroutine(TimeToLoadMain());
        
    }

    protected virtual void MaxLimitReachedAudio(object sender, ControllableEventArgs e)
    {
        
        StartCoroutine(TimeToLoadAudio());
        
    }

    protected virtual void MaxLimitReachedGraphics(object sender, ControllableEventArgs e)
    {
        StartCoroutine(TimeToLoadGraphics());
    }

    IEnumerator TimeToLoadMain()
    {
        main.SetActive(false);
        yield return new WaitForSeconds(seconds);
        settings.SetActive(true);
    }

    IEnumerator TimeToLoadAudio()
    {
        settings.SetActive(false);
        yield return new WaitForSeconds(seconds);
        audioSettings.SetActive(true);
    }

    IEnumerator TimeToLoadGraphics()
    {
        settings.SetActive(false);
        yield return new WaitForSeconds(seconds);
        graphics.SetActive(true);
    }
}
