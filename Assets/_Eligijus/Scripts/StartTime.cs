﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTime : MonoBehaviour
{
    TimeLeft time;
    int count = 0;

    void Start()
    {
        time = GetComponentInParent<TimeLeft>();    
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "[VRTK][AUTOGEN][Controller][NearTouch][CollidersContainer]" && count <= 2)
        {
            time.TimerStart();
            count++;
        }
        //else if (other.name == "[VRTK][AUTOGEN][Controller][NearTouch][CollidersContainer]" && count >= 3)
        //{
            
        //}
    }
}
