﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK.Controllables;
using VRTK.Controllables.PhysicsBased;

public class SoundControllMainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public VRTK_PhysicsSlider controllable;
    SoundVolumePitch volumeControll;
    public Text displayText;
    [SerializeField] bool master;
    [SerializeField] bool music;
    [SerializeField] bool effects;
    float value;
    bool updated = false;
    private void Start()
    {
        volumeControll = (SoundVolumePitch)FindObjectOfType(typeof(SoundVolumePitch));
    }

    void OnEnable()
    {
        controllable = (controllable == null ? GetComponent<VRTK_PhysicsSlider>() : controllable);
        controllable.ValueChanged += ValueChanged;
        if (master)
        {
            if (PlayerPrefs.HasKey("masterVolume"))
            {
                controllable.SetPositionTargetWithStepValue(PlayerPrefs.GetFloat("masterVolume"));
                value = PlayerPrefs.GetFloat("masterVolume");
            } 
            else
            {
                controllable.SetPositionTargetWithStepValue(1);
                value = 1;
            }
        }
        else if (music)
        {
            if (PlayerPrefs.HasKey("musicVolume"))
            {
                controllable.SetPositionTargetWithStepValue(PlayerPrefs.GetFloat("musicVolume"));
                value = PlayerPrefs.GetFloat("musicVolume");
            }
            else
            {
                controllable.SetPositionTargetWithStepValue(1);
                value = 1;
            }
        }
        else if (effects)
        {
            if (PlayerPrefs.HasKey("intercatVolume"))
            {
                controllable.SetPositionTargetWithStepValue(PlayerPrefs.GetFloat("intercatVolume"));
                value = PlayerPrefs.GetFloat("intercatVolume");
            }
            else
            {
                controllable.SetPositionTargetWithStepValue(1);
                value = 1;
            }
        }
        
        

    }

    protected virtual void ValueChanged(object sender, ControllableEventArgs e)
    {
        if (displayText != null)
        {
            displayText.text = e.value.ToString("F1");
        }
        if (master)
        {
            volumeControll.masterVolume = e.value;
            value = e.value;
        }
        else if (music)
        {
            volumeControll.musicVolume = e.value;
            value = e.value;
        }
        else if (effects)
        {
            volumeControll.interactVolume = e.value;
            value = e.value;
        }
    }

    private void Update()
    {
        if (master && value != volumeControll.masterVolume)
        {
            value = volumeControll.masterVolume;
            controllable.SetPositionTargetWithStepValue(value);
        }
        else if (music && value != volumeControll.musicVolume)
        {
            value = volumeControll.musicVolume;
            controllable.SetPositionTargetWithStepValue(value);
        }
        else if (effects && value != volumeControll.interactVolume)
        {
            value = volumeControll.interactVolume;
            controllable.SetPositionTargetWithStepValue(value);
        }
        displayText.text = controllable.GetStepValue(controllable.GetValue()).ToString();


    }


}
