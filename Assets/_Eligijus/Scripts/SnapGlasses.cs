﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using VRTK;
public class SnapGlasses : MonoBehaviour
{
    public VRTK_SnapDropZone snap;
    public PostProcessingProfile beh;
    public PostProcessingBehaviour t;
    GameObject snapedGo;
    bool snaped = false;
    bool oneTime = false;
    bool unSnaped = true;
    private void Start()
    {
        snap = GetComponent<VRTK_SnapDropZone>();
        beh = Camera.main.GetComponent<PostProcessingBehaviour>().profile;
        Debug.Log(beh.colorGrading.settings.channelMixer.blue);
        beh.colorGrading.settings.channelMixer.blue.Set(0, 0, 2);
        Debug.Log(beh.colorGrading.settings.channelMixer.blue);
    }

    // Start is called before the first frame update
    public void StartSnapGlasses()
    {
        for (int i = 0; i < snapedGo.transform.childCount; i++)
        {
            if (i == 5 || i == 6)
            {
                snapedGo.transform.GetChild(i).gameObject.SetActive(false);
            }
            else if(i == 7)
            {
                //snapedGo.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }

    public void DisableSnapGlasses()
    {
        for (int i = 0; i < snapedGo.transform.childCount; i++)
        {
            if (i == 5 || i == 6)
            {
                snapedGo.transform.GetChild(i).gameObject.SetActive(true);
            }
            else if (i == 7)
            {
                snapedGo.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    private void Update()
    {
        if (!snaped && !oneTime && snap.IsSnapped())
        {
            snapedGo = snap.GetCurrentSnappedObject();
            StartSnapGlasses();
            snaped = true;
            oneTime = true;
        }
        else if(snaped && oneTime && !snap.IsSnapped())
        {
            DisableSnapGlasses();
            snaped = false;
            oneTime = false;
        }
    }
}
