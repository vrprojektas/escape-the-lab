﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class GrabbingSound : MonoBehaviour
{
    VRTK_InteractableObject interact;
    bool play = true;
    AudioSource sound;
    // Start is called before the first frame update
    void Start()
    {
        interact = GetComponentInParent<VRTK_InteractableObject>();
        sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (interact.IsGrabbed() && play)
        {
            sound.Play();
            play = false;
        }
        else if (!interact.IsGrabbed())
        {
            play = true;
        }
    }
}
