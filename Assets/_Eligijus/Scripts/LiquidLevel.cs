﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class LiquidLevel : MonoBehaviour
{

    LiquidVolumeAnimator LVA;
    public float level = 0;
    [SerializeField] float LevelOff = 0.0006f;
    Material mat;

    // Start is called before the first frame update
    void Start()
    {
        LVA = GetComponent<LiquidVolumeAnimator>();
        LVA.discardLevel = LevelOff;
        level = LVA.level;

    }

    // Update is called once per frame
    void Update()
    {
        if (VRTK_BasicTeleport.TeleportCheck)
        {
            LVA.enabled = false;
        }
        else
        {
            if (level >= LevelOff)
            {
                LVA.level = level;
                LVA.enabled = true;
            }
            else
            {
                level = 0;
                LVA.level = 0;
                LVA.enabled = false;
            }
        }


    }
}
