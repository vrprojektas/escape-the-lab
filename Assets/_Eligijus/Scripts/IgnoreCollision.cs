﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreCollision : MonoBehaviour
{
    GameObject camera;
    Collider collider;
    bool find = false;
    // Start is called before the first frame update
    void Start()
    {
        collider = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!find)
        {
            camera = GameObject.FindGameObjectWithTag("Head Collider");
            if (camera != null)
            {
                Physics.IgnoreCollision(collider, camera.GetComponent<Collider>());
                find = true;
            }
        }
    }
}
