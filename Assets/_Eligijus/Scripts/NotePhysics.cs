﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotePhysics : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] int speed = 1;
    [SerializeField] float time = 1;
    float temptime = 0;
    float count = 0;
    bool collide = false;

    void Start()
    {
        temptime = time / 4;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(rb.velocity.y);
        if (!collide)
        {
            if (rb.velocity.y < -0.01 && count < temptime * 3 && count > temptime && count > temptime * 2)
            {
                gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x - 0.023f * (speed * 0.4f), gameObject.transform.eulerAngles.y, gameObject.transform.eulerAngles.z);
                count += Time.deltaTime;
            }
            else if (rb.velocity.y < -0.01 && count > temptime * 3 && count < temptime * 4)
            {
                rb.AddForce(-transform.up * speed * 0.6f);
                count += Time.deltaTime;
                gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x - 0.025f * (speed * 0.8f), gameObject.transform.eulerAngles.y, gameObject.transform.eulerAngles.z);
            }
            else if (rb.velocity.y < -0.01 && count < temptime * 2 && count > temptime && count > temptime)
            {
                gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x + 0.022f * (speed * 0.4f), gameObject.transform.eulerAngles.y, gameObject.transform.eulerAngles.z);
                count += Time.deltaTime;
            }
            else if (rb.velocity.y < -0.01 && count < temptime &&  count >= 0)
            {
                rb.AddForce(transform.up * speed * 0.6f);
                count += Time.deltaTime;
                gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x + 0.025f * (speed * 0.8f), gameObject.transform.eulerAngles.y, gameObject.transform.eulerAngles.z);
            }
            else if (rb.velocity.y < -0.01 && count > temptime * 4)
            {
                count = 0;
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        collide = true;
    }

    void OnCollisionExit(Collision collision)
    {
        collide = false;
    }
}