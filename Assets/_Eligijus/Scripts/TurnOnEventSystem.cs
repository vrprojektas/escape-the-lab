﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class TurnOnEventSystem : MonoBehaviour
{
    [SerializeField] GameObject eventSystem;
    [SerializeField] VRTK_UIPointer leftUiPointer;
    [SerializeField] VRTK_UIPointer rightUiPointer;
    [SerializeField] int timeToUpdate;
    int cnt = 0;
    // Update is called once per frame
    void Update()
    {
        if (cnt < timeToUpdate)
        {
            eventSystem.SetActive(true);
            leftUiPointer.enabled = false;
            leftUiPointer.enabled = true;

            rightUiPointer.enabled = false;
            rightUiPointer.enabled = true;
            cnt++;
        }
        else
        {
            Destroy(this);
        }
    }
}
