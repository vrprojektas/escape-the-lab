﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlackScreen : MonoBehaviour
{
    GameObject canvas;
    Image panel;
    bool exited = false;
    [SerializeField] float blackImageTime;
    [SerializeField] int seperateParts;
    float timeCount = 0;
    float increase = 0;
    [SerializeField]float alppha = 0;
    // Start is called before the first frame update
    void Start()
    {
        increase = 100 / seperateParts;
        increase = increase / 100;
    }

    // Update is called once per frame
    void Update()
    {

        if (canvas == null)
        {
            canvas = GameObject.FindGameObjectWithTag("Black Canvas");
            panel = canvas.transform.GetChild(0).gameObject.GetComponent<Image>();
        }
        else
        {
            if (exited)
            {
                if (timeCount < blackImageTime)
                {
                    timeCount += (Time.deltaTime * 10);
                }
                else if (timeCount > blackImageTime && panel.color.a < 1)
                {
                    timeCount = 0;
                    alppha += increase;
                    panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, alppha);
                }
            }
            else
            {
                if (timeCount < blackImageTime)
                {
                    timeCount += (Time.deltaTime * 10);
                }
                else if (timeCount > blackImageTime && panel.color.a > increase)
                {
                    timeCount = 0;
                    alppha -= increase;
                    panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, alppha);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Black Trigger")
        {
            exited = false;

        }
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Black Trigger")
        {
            exited = false;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Black Trigger")
        {
            exited = true;

        }
    }
}
