﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class FireExtingueshes : MonoBehaviour
{
    [SerializeField] List<string> ignoreTagCollision = new string[] { "Fire" }.ToList<string>();
    [SerializeField] List<string> CollisionTag = new string[] { "Water" }.ToList<string>();
    [SerializeField] float timeCheck = 1;
    [SerializeField] ParticleSystem particle;
    AudioSource audio;
    FireOn fire;
    int particleCollision = 0;
    int tempParticeCollision = 0;
    float secTime = 0;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
        fire = gameObject.GetComponent<FireOn>();    
    }

    private void OnParticleCollision(GameObject other)
    {
        Debug.Log(other.tag);
        if (!ignoreTagCollision.Contains(other.tag) && CollisionTag.Contains(other.tag) && fire.Active)
        {
            fire.fireEstinguished = true;
            fire.Active = false;
            particleCollision++;
            if (!audio.isPlaying)
            {
                audio.Play();
                
            }
                particle.Play(true);
        }
    }
    private void Update()
    {
        if (particleCollision != tempParticeCollision)
        {
            tempParticeCollision = particleCollision;
            secTime = 0;
        }
        else if (particleCollision == tempParticeCollision && secTime < timeCheck && fire.fireEstinguished)
        {
            secTime += Time.deltaTime;
        }
        else if (secTime >= timeCheck && fire.fireEstinguished)
        {
            fire.fireEstinguished = false;
            secTime = 0;
        }
    }


}
