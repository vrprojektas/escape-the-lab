﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;

public class GoBackwards : MonoBehaviour
{
    [SerializeField] VRTK_BaseControllable controllableSettings;
    [SerializeField] VRTK_BaseControllable controllableAudio;
    [SerializeField] VRTK_BaseControllable controllablegraphics;
    [SerializeField] GameObject main;
    [SerializeField] GameObject settings;
    [SerializeField] GameObject audioSettings;
    [SerializeField] GameObject graphics;
    [SerializeField] float seconds;
    // Start is called before the first frame update
    protected virtual void OnEnable()
    {
        controllableSettings.MaxLimitReached += MaxLimitReachedSettings;
        controllableAudio.MaxLimitReached += MaxLimitReachedAudio;
        //controllablegraphics.MaxLimitReached += MaxLimitReachedGraphics;
    }

    protected virtual void MaxLimitReachedSettings(object sender, ControllableEventArgs e)
    {
        StartCoroutine(TimeToLoadSettings());   
    }

    protected virtual void MaxLimitReachedAudio(object sender, ControllableEventArgs e)
    {
        StartCoroutine(TimeToLoadAudio());
    }

    protected virtual void MaxLimitReachedGraphics(object sender, ControllableEventArgs e)
    {
        StartCoroutine(TimeToLoadGraphics());
    }

    IEnumerator TimeToLoadSettings()
    {
        settings.SetActive(false);
        yield return new WaitForSeconds(seconds);
        main.SetActive(true);
    }

    IEnumerator TimeToLoadAudio()
    {
        audioSettings.SetActive(false);
        yield return new WaitForSeconds(seconds);
        settings.SetActive(true);
    }

    IEnumerator TimeToLoadGraphics()
    {
        graphics.SetActive(false);
        yield return new WaitForSeconds(seconds);
        settings.SetActive(true);
    }
}
