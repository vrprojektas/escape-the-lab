﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ShadowCascadesUi : MonoBehaviour
{
    TMP_Dropdown dropdown;

    private void OnEnable()
    {
        if (dropdown == null)
        {
            dropdown = GetComponent<TMP_Dropdown>();
        }
        if (QualitySettings.shadowCascades == 0)
        {
            dropdown.value = 0;
        }
        else if (QualitySettings.shadowCascades == 2)
        {
            dropdown.value = 1;
        }
        else if (QualitySettings.shadowCascades == 4)
        {
            dropdown.value = 2;
        }
    }

    public void SetShadowCascades(int index)
    {
        if (index == 0)
        {
            QualitySettings.shadowCascades = 0;
        }
        else if (index == 1)
        {
            QualitySettings.shadowCascades = 2;
        }
        else if (index == 2)
        {
            QualitySettings.shadowCascades = 4;
        }
        UpdateSettings.Save = true;
    }
}
