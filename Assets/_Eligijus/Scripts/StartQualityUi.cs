﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class StartQualityUi : MonoBehaviour
{
    TMP_Dropdown dropdown;
    // Start is called before the first frame update

    private void OnEnable()
    {
        if (dropdown != null)
        {
            dropdown.value = QualitySettings.GetQualityLevel();
        }
    }

    void Start()
    {
        dropdown = GetComponent<TMP_Dropdown>();
        dropdown.ClearOptions();
        List<string> options = new List<string>();

        for (int i = 0; i < QualitySettings.names.Length; i++)
        {
            options.Add(QualitySettings.names[i]);
        }
        dropdown.AddOptions(options);
        dropdown.value = QualitySettings.GetQualityLevel();
        
    }

    public void ChangeQuality(int index)
    {
        QualitySettings.SetQualityLevel(index, true);
        transform.parent.gameObject.SetActive(false);
        transform.parent.gameObject.SetActive(true);
        UpdateSettings.Save = true;
    }

}
