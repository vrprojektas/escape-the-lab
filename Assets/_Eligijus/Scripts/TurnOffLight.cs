﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class TurnOffLight : MonoBehaviour
{
    [SerializeField] GameObject[] lights;
    [SerializeField] GameObject[] ignoreCollision;
    [SerializeField] float angleTarget = 96f;
    [SerializeField] TurnOnLight on;
    VRTK_InteractableObject interact;
    GameObject lightSwitch;
    public bool lightsOff = false;

    public GameObject[] litObjs;
    public GameObject[] unlitObjs;

    ButtonSound buttonSound;
    // Start is called before the first frame update
    void Start()
    {
        buttonSound = GetComponent<ButtonSound>();
        lightSwitch = transform.parent.gameObject;
        interact = GetComponent<VRTK_InteractableObject>();
        foreach (GameObject collision in ignoreCollision)
        {
            Physics.IgnoreCollision(collision.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }

    void Update()
    {
        if (interact.IsTouched() && !lightsOff)
        {
            if (lightSwitch.transform.localRotation.eulerAngles.z == 90)
            {
                lightSwitch.transform.Rotate(0, 0, 6);
            }
            else if (lightSwitch.transform.localRotation.eulerAngles.z < 90)
            {
                lightSwitch.transform.Rotate(0, 0, 12);
            }
            for (int i = 0; i < lights.Length; i++)
            {
                Debug.Log("gg");
                lights[i].SetActive(false);
            }
            on.lightsOn = false;
            lightsOff = true;
            buttonSound.check = true;
            foreach (GameObject litObj in litObjs)
            {
                litObj.SetActive(false);
            }
            foreach (GameObject unlitObj in unlitObjs)
            {
                unlitObj.SetActive(true);
            }
        }
    }
    // Update is called once per frame
    void OnCollisionEnter(Collision other)
    {
        if (!lightsOff)
        {
            if (lightSwitch.transform.localRotation.eulerAngles.z == 90)
            {
                lightSwitch.transform.Rotate(0, 0, 6);
            }
            else if (lightSwitch.transform.localRotation.eulerAngles.z < 90)
            {
                lightSwitch.transform.Rotate(0, 0, 12);
            }
            foreach (GameObject light in lights)
            {
                light.SetActive(false);
            }
            on.lightsOn = false;
            lightsOff = true;
            buttonSound.check = true;
            foreach (GameObject litObj in litObjs)
            {
                litObj.SetActive(false);
            }
            foreach (GameObject unlitObj in unlitObjs)
            {
                unlitObj.SetActive(true);
            }
        }
    }
}
