﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseSound : MonoBehaviour
{
    AudioSource audio;
    bool open = true;
    int cnt = 0;
    [SerializeField]CloseDoors close;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (close.isClosing && open)
        {
            audio.Play();
            open = false;
        }
    }
}
