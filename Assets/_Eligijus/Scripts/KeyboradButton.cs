﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using VRTK.Controllables.PhysicsBased;
using VRTK.Controllables;
using UnityEditor;
using TMPro;
public class KeyboradButton : MonoBehaviour
{
    public UnityEvent unityEvent;
    public bool letter = true;
    public int times = 1;
    AudioSource buttonSound;
    UserNameController controller;
    VRTK_PhysicsPusher buttonClick;
    int tempTimes = 0;
    TextMeshPro tmPro;
    // Start is called before the first frame update
    void Awake()
    {
        if (unityEvent == null && !letter)
        {
            unityEvent = new UnityEvent();
        }
    }

    void Start()
    {
        controller = transform.parent.parent.GetComponent<UserNameController>();
        tmPro = GetComponentInChildren<TextMeshPro>();
        
    }

    private void OnEnable()
    {
        if (buttonSound == null)
        {
            buttonSound = GetComponent<AudioSource>();
        }
        if (buttonClick == null)
        {
            buttonClick = GetComponent<VRTK_PhysicsPusher>();
        }
        buttonClick.MinLimitReached += MaxLimitReached;
    }

    protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        tempTimes++;
        if (letter && tempTimes > times)
        {
            controller.AddCharacter(tmPro.text);
            buttonSound.Play();
        }
        else if(tempTimes > times)
        {
            unityEvent.Invoke();
            buttonSound.Play();
        }
    }
}


//[CustomEditor(typeof(KeyboradButton))]
//public class KeyboradButtonEditor : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        var myScript = target as KeyboradButton;

//        myScript.letter = GUILayout.Toggle(myScript.letter, "Letter");

//        if (!myScript.letter)
//        {
//            this.serializedObject.Update();
//            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("unityEvent"), true);
//            this.serializedObject.ApplyModifiedProperties();
//        }

//    }
//}
