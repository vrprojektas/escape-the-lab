﻿using System.Collections;
using System.IO;
using UnityEngine;

public class Paintable : MonoBehaviour
{
    static int NameCount = 0;
    public Transform mainTransform;
    float BrushSize = 1;
    Vector2 offset;
    Material mat;
    public Color color;
    public Texture2D t2d;
    Vector2 CalculateStart = Vector2.zero;
    MeshRenderer rt;
    [SerializeField] bool useXZ = false;
    [SerializeField] bool useXZDifferentStart = false;
    [SerializeField] bool useZY = false;
    [SerializeField] int pixelH = 256;
    [SerializeField] int pixelW = 256;
    // Use this for initialization
    void Start()
    {
        t2d = new Texture2D(pixelW, pixelH);
        t2d.name = "DrawingTexture" + NameCount;
        for (int i = 0; i < pixelW; ++i)
        {
            for (int a = 0; a < pixelH; a++)
            {
                t2d.SetPixel(i, a, Color.white);
            }
        }
        t2d.Apply();


        NameCount++;
        rt = mainTransform.GetComponent<MeshRenderer>();
        mat = rt.material;
    }



    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.GetComponent<PenDraw>() != null)
        {
            PenDraw drawSettings = collision.gameObject.GetComponent<PenDraw>();
            offset = drawSettings.GetOffset();
            color = drawSettings.GetColor();
            BrushSize = drawSettings.GetBrushStrenght();
            BrushSize = BrushSize - 1;
            //Vector2 viewPos = gameObject.transform.TransformPoint(contact.point);
            //Vector2 viewPos = mainTransform.InverseTransformPoint(contact.point);
            //Vector3 gg = mainTransform.TransformPoint(contact.otherCollider.transform.position);
            Vector3 max = rt.bounds.max * 1000;
            Vector3 min = rt.bounds.min * 1000;

            Debug.Log(max);
            Debug.Log(min);
            Vector3 viewPos = Vector3.zero;
            Vector2 calculated = Vector2.zero;
            if (useXZ)
            {
                CalculateStart = new Vector2(max.x, min.z);

                float heigth = max.x - min.x;
                float width = max.z - min.z;

                viewPos = collision.GetContact(0).point * 1000;
                Vector2 pos = new Vector2(viewPos.x, viewPos.z);
                pos = CalculateStart - pos;
                pos = new Vector2(pos.x, -pos.y);
                
                calculated = new Vector2((int)((pos.y + offset.x) * (pixelW / width)), (int)((pos.x + offset.y) * (pixelH / heigth)));
                Debug.Log(pos);
            }
            else if (useXZDifferentStart)
            {
                CalculateStart = new Vector2(max.x, min.z);

                float heigth = max.x - min.x;
                float width = max.z - min.z;

                viewPos = collision.GetContact(0).point * 1000;
                Vector2 pos = new Vector2(viewPos.x, viewPos.z);
                pos = CalculateStart - pos;
                pos = new Vector2(pos.x, -pos.y);

                calculated = new Vector2((int)((pos.y + offset.x) * (pixelW / width)), (int)((pos.x + offset.y) * (pixelH / heigth)));
                //Debug.Log(pos);
            }
            else if(useZY) {

                CalculateStart = new Vector2(max.z, min.y);

                float heigth = max.y - min.y;
                float width = max.z - min.z;

                viewPos = collision.GetContact(0).point * 1000;
                Vector2 pos = new Vector2(viewPos.z, viewPos.y);
                pos = CalculateStart - pos;
                pos = new Vector2(pos.x, -pos.y);

                calculated = new Vector2((int)((pos.x + offset.x) * (pixelW / width)), (int)((pos.y + offset.y) * (pixelH / heigth)));

                Debug.Log(pos);
            }

            if (BrushSize > 0)
            {
                for (int i = 1; i <= BrushSize; i++)
                {

                    t2d.SetPixel((int)calculated.x, (int)calculated.y, color);

                    if (calculated.x + i < pixelW && calculated.y + i < pixelH)
                    {
                        t2d.SetPixel((int)calculated.x + i, (int)calculated.y + i, color);
                    }
                    if (calculated.x - i > 0 && calculated.y - i > 0)
                    {
                        t2d.SetPixel((int)calculated.x - i, (int)calculated.y - i, color);
                    }
                    if (calculated.x - i > 0 && calculated.y + i < pixelH)
                    {
                        t2d.SetPixel((int)calculated.x - i, (int)calculated.y + i, color);
                    }
                    if (calculated.x + i < pixelW && calculated.y - i > 0)
                    {
                        t2d.SetPixel((int)calculated.x + i, (int)calculated.y - i, color);
                    }
                    if (calculated.x + i < pixelW)
                    {
                        t2d.SetPixel((int)calculated.x + i, (int)calculated.y, color);
                    }
                    if (calculated.x - i < pixelW)
                    {
                        t2d.SetPixel((int)calculated.x - i, (int)calculated.y, color);
                    }
                    if (calculated.y + i < pixelH)
                    {
                        t2d.SetPixel((int)calculated.x, (int)calculated.y + i, color);
                    }
                    if (calculated.y - i > 0)
                    {
                        t2d.SetPixel((int)calculated.x, (int)calculated.y - i, color);
                    }

                }
            }
            else
            {
                t2d.SetPixel((int)calculated.x, (int)calculated.y, color);
            }

            t2d.Apply();
            mat.SetTexture("_SecTex", t2d); // _SecTex
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.GetComponent<PenDraw>() != null)
        {
            PenDraw drawSettings = collision.gameObject.GetComponent<PenDraw>();
            offset = drawSettings.GetOffset();
            color = drawSettings.GetColor();
                //Vector2 viewPos = gameObject.transform.TransformPoint(contact.point);
            Vector3 max = rt.bounds.max * 1000;
            Vector3 min = rt.bounds.min * 1000;

            Vector3 viewPos = Vector3.zero;
            Vector2 calculated = Vector2.zero;

            if (useXZ)
            {
                CalculateStart = new Vector2(max.x, min.z);

                float heigth = max.x - min.x;
                float width = max.z - min.z;

                viewPos = collision.GetContact(0).point * 1000;
                Vector2 pos = new Vector2(viewPos.x, viewPos.z);
                pos = CalculateStart - pos;
                pos = new Vector2(pos.x, -pos.y);

                calculated = new Vector2((int)((pos.y + offset.x) * (pixelW / width)), (int)((pos.x + offset.y) * (pixelH / heigth)));



            }
            else if (useXZDifferentStart)
            {
                CalculateStart = new Vector2(max.x, min.z);

                float heigth = max.x - min.x;
                float width = max.z - min.z;

                viewPos = collision.GetContact(0).point * 1000;
                Vector2 pos = new Vector2(viewPos.x, viewPos.z);
                pos = CalculateStart - pos;
                pos = new Vector2(pos.x, -pos.y);

                calculated = new Vector2((int)((pos.y + offset.x) * (pixelW / width)), (int)((pos.x + offset.y) * (pixelH / heigth)));
                Debug.Log(pos);
            }
            else if(useZY) {


                CalculateStart = new Vector2(max.z, min.y);

                float heigth = max.y - min.y;
                float width = max.z - min.z;

                viewPos = collision.GetContact(0).point * 1000;
                Vector2 pos = new Vector2(viewPos.z, viewPos.y);
                pos = CalculateStart - pos;
                pos = new Vector2(pos.x, -pos.y);

                calculated = new Vector2((int)((pos.x + offset.x) * (pixelW / width)), (int)((pos.y + offset.y) * (pixelH / heigth)));

                Debug.Log(pos);

            }
            //Debug.Log(calculated);
            //Debug.Log(viewPos);
            //Debug.Log(calculated);
            if (BrushSize > 0)
            {
                for (int i = 1; i <= BrushSize; i++)
                {

                    t2d.SetPixel((int)calculated.x, (int)calculated.y, color);

                    if (calculated.x + i < pixelW && calculated.y + i < pixelH)
                    {
                        t2d.SetPixel((int)calculated.x + i, (int)calculated.y + i, color);
                    }
                    if (calculated.x - i > 0 && calculated.y - i > 0)
                    {
                        t2d.SetPixel((int)calculated.x - i, (int)calculated.y - i, color);
                    }
                    if (calculated.x - i > 0 && calculated.y + i < pixelH)
                    {
                        t2d.SetPixel((int)calculated.x - i, (int)calculated.y + i, color);
                    }
                    if (calculated.x + i < pixelW && calculated.y - i > 0)
                    {
                        t2d.SetPixel((int)calculated.x + i, (int)calculated.y - i, color);
                    }
                    if (calculated.x + i < pixelW)
                    {
                        t2d.SetPixel((int)calculated.x + i, (int)calculated.y, color);
                    }
                    if (calculated.x - i < pixelW)
                    {
                        t2d.SetPixel((int)calculated.x - i, (int)calculated.y, color);
                    }
                    if (calculated.y + i < pixelH)
                    {
                        t2d.SetPixel((int)calculated.x, (int)calculated.y + i, color);
                    }
                    if (calculated.y - i > 0)
                    {
                        t2d.SetPixel((int)calculated.x, (int)calculated.y - i, color);
                    }

                }
            }
            else
            {
                t2d.SetPixel((int)calculated.x, (int)calculated.y, color);
            }
                t2d.Apply();
                mat.SetTexture("_SecTex", t2d); // _SecTex

        }
    }

}