﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using VRTK.Controllables.PhysicsBased;

public class ButtonClick : MonoBehaviour
{



    // Start is called before the first frame update
    GameObject leftGo;
    GameObject rightGo;
    VRTK_ControllerEvents left;
    VRTK_ControllerEvents right;
    GameObject headCollider;

    // left work with controlls
    VRTK_InteractTouch leftTouch;
    VRTK_InteractGrab leftGrab;
    VRTK_InteractUse leftUse;
    VRTK_InteractNearTouch leftNearTouch;
    VRTK_Pointer leftPointer;
    VRTK_BezierPointerRenderer bezierLeftRender;
    VRTK_StraightPointerRenderer leftRender;

    // right work with controlls;
    VRTK_InteractTouch rightTouch;
    VRTK_InteractGrab rightGrab;
    VRTK_InteractUse rightUse;
    VRTK_InteractNearTouch rightNearTouch;
    VRTK_Pointer rightPointer;
    VRTK_BezierPointerRenderer bezierRightRender;
    VRTK_StraightPointerRenderer rightRender;

    VRTK_ControllerEvents.ButtonAlias activationButton;
    
    bool realised1 = false;
    bool realised2 = true;
    int clickCount = 0;
    bool on = false;
    int updateCnt = 0;

    [SerializeField] GameObject goTeleport;
    [SerializeField] Vector3 teleportPosition;
    Vector3 previosPosition;

    [SerializeField] GameObject menu;
    [SerializeField] GameObject blackOut;
    [SerializeField] float time;
    [SerializeField] int chunks;
    float timePerChunk = 0;
    float chunkCnt = 0;
    float timeCnt = 0;
    float incrise = 0;
    float alpha = 0;
    [SerializeField] TimeLeft[] timer;
    [SerializeField] float offset;

    [SerializeField] GameObject[] disableWhenReturn;
    [SerializeField] GameObject enableWhenReturn;

    GameObject rightPointLine;
    GameObject leftPointLine;

    public bool backToGame = false;

    void Start()
    {
        menu.gameObject.SetActive(false);
        blackOut.SetActive(true);
        timePerChunk = time / chunks;
        incrise = 97.5f / chunks;
        incrise = incrise / 100;
        foreach (GameObject controlls in GameObject.FindGameObjectsWithTag("GameController"))
        {
            if (controlls.name == "RightControllerScriptAlias")
            {
                rightGo = controlls;
                right = controlls.GetComponent<VRTK_ControllerEvents>();
                rightTouch = right.GetComponent<VRTK_InteractTouch>();
                rightGrab = right.GetComponent<VRTK_InteractGrab>();
                rightUse = right.GetComponent<VRTK_InteractUse>();
                rightNearTouch = right.GetComponent<VRTK_InteractNearTouch>();
                rightPointer = right.GetComponent<VRTK_Pointer>();
                bezierRightRender = right.GetComponent<VRTK_BezierPointerRenderer>();
                rightPointer.pointerRenderer = bezierRightRender;
                rightRender = right.GetComponent<VRTK_StraightPointerRenderer>();
            }
            if (controlls.name == "LeftControllerScriptAlias")
            {
                leftGo = controlls;
                left = controlls.GetComponent<VRTK_ControllerEvents>();
                leftTouch = left.GetComponent<VRTK_InteractTouch>();
                leftGrab = left.GetComponent<VRTK_InteractGrab>();
                leftUse = left.GetComponent<VRTK_InteractUse>();
                leftNearTouch = left.GetComponent<VRTK_InteractNearTouch>();
                leftPointer = left.GetComponent<VRTK_Pointer>();
                bezierLeftRender = left.GetComponent<VRTK_BezierPointerRenderer>();
                leftPointer.pointerRenderer = bezierLeftRender;
                leftRender = left.GetComponent<VRTK_StraightPointerRenderer>();

            }
        }
        activationButton = rightPointer.activationButton;
    }

    // Update is called once per frame
    void Update()
    {
        if (headCollider == null || rightPointLine == null || leftPointLine == null)
        {
            headCollider = GameObject.FindGameObjectWithTag("Head Collider");
            rightPointLine = GameObject.Find("[VRTK][AUTOGEN][RightControllerScriptAlias][BasePointerRenderer_Origin_Smoothed]");
            leftPointLine = GameObject.Find("[VRTK][AUTOGEN][LeftControllerScriptAlias][BasePointerRenderer_Origin_Smoothed]");
        }
        else if(headCollider != null && rightPointLine != null && leftPointLine != null)
        {

            if (on && updateCnt < 3)
            {
                Vector3 pos = menu.transform.forward * offset + Camera.main.transform.position;
                menu.transform.position = pos;
                updateCnt++;
            }
            if ((left.IsButtonPressed(VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress) || right.IsButtonPressed(VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress)) && clickCount == 0 && realised2)
            {
                timeCnt = 0;
                chunkCnt = 0;
                //Vector3 pos = menu.transform.forward * offset + Camera.main.transform.position;
                //menu.transform.position = pos; // new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
                //menu.transform.rotation = new Quaternion(menu.transform.rotation.x, Camera.main.transform.rotation.y, menu.transform.rotation.z, menu.transform.rotation.w);

                SetRotate(menu.gameObject, Camera.main.gameObject);
                clickCount++;
            }
            else if(!on && chunks == chunkCnt && clickCount == 1)
            {
                menu.gameObject.SetActive(true);
                previosPosition = goTeleport.transform.position;
                Teleport();
                blackOut.SetActive(false);
                DisableControlls();
            }
            else if (clickCount == 1 && realised2 && timeCnt < timePerChunk && chunkCnt < chunks)
            {

                timeCnt += (Time.deltaTime * 10);
            }
            else if (clickCount == 1 && realised2 && timeCnt > timePerChunk && chunkCnt < chunks && blackOut.GetComponent<Image>().color.a < 0.975)
            {
                timeCnt = 0;
                chunkCnt++;
                alpha += incrise;
                blackOut.GetComponent<Image>().color = new Color(blackOut.GetComponent<Image>().color.r, blackOut.GetComponent<Image>().color.g, blackOut.GetComponent<Image>().color.b, alpha);
            }
            else if ((!left.IsButtonPressed(VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress) && !right.IsButtonPressed(VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress) && !backToGame) && clickCount == 1)
            {
                realised1 = true;
                realised2 = false;
            }
            else if ((left.IsButtonPressed(VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress) || right.IsButtonPressed(VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress) || backToGame) && clickCount == 1 && realised1)
            {
                updateCnt = 0;
                clickCount = 0;
                timeCnt = 0;
                chunkCnt = 0;
                Teleport();
                blackOut.SetActive(true);
                EnableControlls();
                menu.gameObject.SetActive(false);
                for (int i = 0; i < disableWhenReturn.Length; i++)
                {
                    disableWhenReturn[i].SetActive(false);
                }
                enableWhenReturn.SetActive(true);
                backToGame = false;
            }
            else if (clickCount == 0 && realised1 && timeCnt < timePerChunk && chunkCnt < chunks)
            {
                timeCnt += (Time.deltaTime * 10);
            }
            else if (clickCount == 0 && realised1 && timeCnt > timePerChunk && chunkCnt < chunks && blackOut.GetComponent<Image>().color.a > incrise)
            {
                timeCnt = 0;
                chunkCnt++;
                alpha -= incrise;
                blackOut.GetComponent<Image>().color = new Color(blackOut.GetComponent<Image>().color.r, blackOut.GetComponent<Image>().color.g, blackOut.GetComponent<Image>().color.b, alpha);
            }
            else if ((!left.IsButtonPressed(VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress) && !right.IsButtonPressed(VRTK_ControllerEvents.ButtonAlias.ButtonTwoPress)) && clickCount == 0)
            {
                realised2 = true;
                realised1 = false;
                backToGame = false;
            }
            
        }
       
    }

    void EnableControlls()
    {
        on = false;
        if (timer.Length > 0)
        {
            for (int i = 0; i < timer.Length; i++)
            { 
                timer[i].TimerStart();
            }
        }
        // right
        rightTouch.enabled = true;
        rightGrab.enabled = true;
        rightUse.enabled = true;
        rightNearTouch.enabled = true;
        // pointer
        rightRender.enabled = false;
        rightPointer.pointerRenderer = bezierRightRender;
        rightPointer.enableTeleport = true;
        rightPointer.holdButtonToActivate = true;
        rightPointer.activateOnEnable = false;
        rightPointer.activationButton = activationButton;
        rightPointer.enabled = false;
        rightPointer.enabled = true;
        for (int i = 0; i < rightGo.transform.childCount; i++)
        {
            rightGo.transform.GetChild(i).gameObject.SetActive(true);
        }

        // left
        leftTouch.enabled = true;
        leftGrab.enabled = true;
        leftUse.enabled = true;
        leftNearTouch.enabled = true;
        // pointer
        leftRender.enabled = false;
        leftPointer.pointerRenderer = bezierLeftRender;
        leftPointer.enableTeleport = true;
        leftPointer.holdButtonToActivate = true;
        leftPointer.activateOnEnable = false;
        leftPointer.activationButton = activationButton;
        leftPointer.enabled = false;
        leftPointer.enabled = true;
        for (int i = 0; i < leftGo.transform.childCount; i++)
        {
            leftGo.transform.GetChild(i).gameObject.SetActive(true);
        }

        headCollider.SetActive(true);
    }

    void DisableControlls()
    {
        on = true;
        if (timer.Length > 0)
        {
            for (int i = 0; i < timer.Length; i++)
            {
                timer[i].StopTimer();
            }
        }
        // right
        rightTouch.enabled = false;
        rightGrab.enabled = false;
        rightUse.enabled = false;
        rightNearTouch.enabled = false;

        // pointer
        rightRender.enabled = true;
        rightPointer.pointerRenderer = rightRender;
        rightPointer.enableTeleport = false;
        rightPointer.holdButtonToActivate = false;
        rightPointer.activateOnEnable = true;
        rightPointer.activationButton = VRTK_ControllerEvents.ButtonAlias.Undefined;
        rightPointer.enabled = false;
        rightPointer.enabled = true;
        for (int i = 0; i < rightGo.transform.childCount; i++)
        {
            rightGo.transform.GetChild(i).gameObject.SetActive(false);
        }

        // left
        leftTouch.enabled = false;
        leftGrab.enabled = false;
        leftUse.enabled = false;
        leftNearTouch.enabled = false;

        // pointer
        leftRender.enabled = true;
        leftPointer.pointerRenderer = leftRender;
        leftPointer.enableTeleport = false;
        leftPointer.holdButtonToActivate = false;
        leftPointer.activateOnEnable = true;
        leftPointer.activationButton = VRTK_ControllerEvents.ButtonAlias.Undefined;
        leftPointer.enabled = false;
        leftPointer.enabled = true;

        for (int i = 0; i < leftGo.transform.childCount; i++)
        {
            leftGo.transform.GetChild(i).gameObject.SetActive(false);
        }
        headCollider.SetActive(false);
    }

    void SetRotate(GameObject toRotate, GameObject camera)
    {
        transform.rotation = Quaternion.LerpUnclamped(toRotate.transform.rotation, new Quaternion(toRotate.transform.rotation.x, camera.transform.rotation.y, toRotate.transform.rotation.z, camera.transform.rotation.w), 1f);
    }

    public bool IsOn()
    {
        return on;
    }

    void Teleport()
    {
        if (on)
        {
            goTeleport.transform.position = previosPosition;
        }
        else
        {
            goTeleport.transform.position = teleportPosition;
        }
    }

}
