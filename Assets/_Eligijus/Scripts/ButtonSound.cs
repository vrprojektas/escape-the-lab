﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables.PhysicsBased;
using UnityEngine.Audio;

public class ButtonSound : MonoBehaviour
{
    VRTK_PhysicsPusher button;
    AudioSource audio;
    //bool check = false;
    public bool check = false;

    void Start()
    {
        button = GetComponent<VRTK_PhysicsPusher>();
        audio = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        if (check)
        {
            audio.Play();
            check = false;
        }
    }
}
