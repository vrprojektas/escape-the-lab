﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropSound : MonoBehaviour
{
    int cnt = 0;
    AudioSource aud;
    // Start is called before the first frame update
    void Start()
    {
        aud = GetComponent<AudioSource>();
    }

    public void PlaySound()
    {
        if (!aud.isPlaying)
        {
            aud.Play();
        }
    }
}
