﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class TurnOnLight : MonoBehaviour
{
    [SerializeField] GameObject[] lights;
    [SerializeField] GameObject[] ignoreCollision;
    [SerializeField] float angleTarget = 84f;
    [SerializeField] TurnOffLight off;
    VRTK_InteractableObject interact;
    GameObject lightSwitch;
    public bool lightsOn = true;

    ButtonSound buttonSound;
    public GameObject[] litObjs;
    public GameObject[] unlitObjs;

    // Start is called before the first frame update
    void Start()
    {
        buttonSound = GetComponent<ButtonSound>();
        lightSwitch = transform.parent.gameObject;
        interact = GetComponent<VRTK_InteractableObject>();
        foreach (GameObject collision in ignoreCollision)
        {
            Physics.IgnoreCollision(collision.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }
    
    void Update()
    {
        if (interact.IsTouched() && off.lightsOff && !lightsOn)
        {
            if (lightSwitch.transform.localRotation.eulerAngles.z == 90)
            {
                lightSwitch.transform.Rotate(0, 0, -6);
            }
            else if (lightSwitch.transform.localRotation.eulerAngles.z > 90)
            {
                lightSwitch.transform.Rotate(0, 0, -12);
            }
            foreach (GameObject light in lights)
            {
                light.SetActive(true);
            }
            off.lightsOff = false;
            lightsOn = true;
            buttonSound.check = true;
            foreach (GameObject litObj in litObjs)
            {
                litObj.SetActive(true);
            }
            foreach (GameObject unlitObj in unlitObjs)
            {
                unlitObj.SetActive(false);
            }

        }
    }
    // Update is called once per frame
    void OnCollisionEnter(Collision other)
    {
        if (off.lightsOff && !lightsOn)
        {
            if (lightSwitch.transform.localRotation.eulerAngles.z == 90)
            {
                lightSwitch.transform.Rotate(0, 0, -6);
            }
            else if (lightSwitch.transform.localRotation.eulerAngles.z > 90)
            {
                lightSwitch.transform.Rotate(0, 0, -12);
            }
            for (int i = 0; i < lights.Length; i++)
            {
                Debug.Log("gg");
                lights[i].SetActive(true);
            }
            off.lightsOff = false;
            lightsOn = true;
            buttonSound.check = true;
            foreach (GameObject litObj in litObjs)
            {
                litObj.SetActive(true);
            }
            foreach (GameObject unlitObj in unlitObjs)
            {
                unlitObj.SetActive(false);
            }
        }
    }
}
