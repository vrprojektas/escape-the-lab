﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerIgnoreCollider : MonoBehaviour
{
    [SerializeField] GameObject[] tableObj;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            for (int a = 0; a < tableObj.Length; a++)
            {
                Physics.IgnoreCollision(gameObject.transform.GetChild(i).gameObject.GetComponent<Collider>(), tableObj[a].GetComponent<Collider>());
            }
        }
    }

}
