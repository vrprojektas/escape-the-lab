﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;

public class KeyForKeypadRemove : MonoBehaviour
{
    [SerializeField] VRTK_BaseControllable controllable;
    [SerializeField] LockUnlockWithPin lockController;
    ButtonSound sound;
    int count = 0;
    public int outputKey = 0;

    private void Start()
    {
        sound = GetComponent<ButtonSound>();
    }

    protected virtual void OnEnable()
    {
        controllable = (controllable == null ? GetComponent<VRTK_BaseControllable>() : controllable);

        controllable.MinLimitReached += MaxLimitReached;
    }


    protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        if (outputKey != -1 && count < 2)
        {
            count++;
        }
        if (outputKey != -1 && count > 1)
        {
            lockController.Del();
            sound.check = true;
        }
    }
}
