﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DryIceCheck : MonoBehaviour
{
    [SerializeField] string tag;
    [SerializeField] ParticleSystem ps;
    public LiquidVolumeAnimator lva;
    [SerializeField] BottleSmash bottle;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == tag)
        {
            collision.gameObject.GetComponent<Melting>().lva = lva;
            collision.gameObject.GetComponent<Collider>().isTrigger = true;
            collision.gameObject.GetComponent<Melting>().pscheck = ps;
            collision.gameObject.GetComponent<Melting>().collided = true;
            collision.gameObject.GetComponent<Melting>().smash = bottle;
        }
    }
}