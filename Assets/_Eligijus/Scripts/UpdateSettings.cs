﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateSettings : MonoBehaviour
{

    public static bool Save = false;
    
    private void Awake()
    {
        if (PlayerPrefs.HasKey("Saves"))
        {

            if (PlayerPrefs.HasKey("WindowMode"))
            {
                if (PlayerPrefs.GetInt("WindowMode") == 0)
                {
                    Screen.fullScreen = true;
                    StartResolutionUi.fullScreen = true;
                }
                else
                {
                    Screen.fullScreen = false;
                    StartResolutionUi.fullScreen = false;
                }
            }

            Resolution[] resolutions = Screen.resolutions;
            for (int i = 0; i < resolutions.Length; i++)
            {
                if (PlayerPrefs.GetString("ResolutionSave") == resolutions[i].width + "X" + resolutions[i].height)
                {
                    Screen.SetResolution(resolutions[i].width, resolutions[i].height, Screen.fullScreen);
                }
            }

            for (int i = 0; i < 4; i++)
            {
                QualitySettings.SetQualityLevel(i, true);
                if (i == 0)
                {

                    // shadow resolution
                    if (PlayerPrefs.GetInt("LowShadowQualitySave") == 0)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.Low;
                    }
                    else if (PlayerPrefs.GetInt("LowShadowQualitySave") == 1)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.Medium;
                    }
                    else if (PlayerPrefs.GetInt("LowShadowQualitySave") == 2)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.High;
                    }
                    else if (PlayerPrefs.GetInt("LowShadowQualitySave") == 3)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.VeryHigh;
                    }

                    if (PlayerPrefs.GetInt("LowShadowsSave") == 0)
                    {
                        QualitySettings.shadows = ShadowQuality.HardOnly;
                    }
                    else if (PlayerPrefs.GetInt("LowShadowsSave") == 1)
                    {
                        QualitySettings.shadows = ShadowQuality.All;
                    }

                    // shadow cascade

                    if (PlayerPrefs.GetInt("LowShadowCascadesSave") == 0)
                    {
                        QualitySettings.shadowCascades = 0;
                    }
                    else if (PlayerPrefs.GetInt("LowShadowCascadesSave") == 1)
                    {
                        QualitySettings.shadowCascades = 2;
                    }
                    else if (PlayerPrefs.GetInt("LowShadowCascadesSave") == 2)
                    {
                        QualitySettings.shadowCascades = 4;
                    }


                    // shadow distance

                    QualitySettings.shadowDistance = PlayerPrefs.GetFloat("LowShadowDistanceSave");

                    // anti-aliasing

                    QualitySettings.antiAliasing = PlayerPrefs.GetInt("LowAntiAliasingSave");

                    // texture resolution

                    if (PlayerPrefs.GetInt("LowSoftParticlesSave") == 0)
                    {
                        QualitySettings.softParticles = false;
                    }
                    else if (PlayerPrefs.GetInt("LowSoftParticlesSave") == 1)
                    {
                        QualitySettings.softParticles = true;
                    }
                }

                else if (i == 1)
                {
                   
                    // shadow resolution
                    if (PlayerPrefs.GetInt("MediumShadowQualitySave") == 0)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.Low;
                    }
                    else if (PlayerPrefs.GetInt("MediumShadowQualitySave") == 1)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.Medium;
                    }
                    else if (PlayerPrefs.GetInt("MediumShadowQualitySave") == 2)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.High;
                    }
                    else if (PlayerPrefs.GetInt("MediumShadowQualitySave") == 3)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.VeryHigh;
                    }

                    if (PlayerPrefs.GetInt("MediumShadowsSave") == 0)
                    {
                        QualitySettings.shadows = ShadowQuality.HardOnly;
                    }
                    else if (PlayerPrefs.GetInt("MediumShadowsSave") == 1)
                    {
                        QualitySettings.shadows = ShadowQuality.All;
                    }

                    // shadow cascade

                    if (PlayerPrefs.GetInt("MediumShadowCascadesSave") == 0)
                    {
                        QualitySettings.shadowCascades = 0;
                    }
                    else if (PlayerPrefs.GetInt("MediumShadowCascadesSave") == 1)
                    {
                        QualitySettings.shadowCascades = 2;
                    }
                    else if (PlayerPrefs.GetInt("MediumShadowCascadesSave") == 2)
                    {
                        QualitySettings.shadowCascades = 4;
                    }


                    // shadow distance

                    QualitySettings.shadowDistance = PlayerPrefs.GetFloat("MediumShadowDistanceSave");

                    // anti-aliasing

                    QualitySettings.antiAliasing = PlayerPrefs.GetInt("MediumAntiAliasingSave");

                    // texture resolution

                    if (PlayerPrefs.GetInt("MediumSoftParticlesSave") == 0)
                    {
                        QualitySettings.softParticles = false;
                    }
                    else if (PlayerPrefs.GetInt("MediumSoftParticlesSave") == 1)
                    {
                        QualitySettings.softParticles = true;
                    }
                }

                else if (i == 2)
                {
                    // shadow resolution
                    if (PlayerPrefs.GetInt("HighShadowQualitySave") == 0)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.Low;
                    }
                    else if (PlayerPrefs.GetInt("HighShadowQualitySave") == 1)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.Medium;
                    }
                    else if (PlayerPrefs.GetInt("HighShadowQualitySave") == 2)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.High;
                    }
                    else if (PlayerPrefs.GetInt("HighShadowQualitySave") == 3)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.VeryHigh;
                    }

                    if (PlayerPrefs.GetInt("HighShadowsSave") == 0)
                    {
                        QualitySettings.shadows = ShadowQuality.HardOnly;
                    }
                    else if (PlayerPrefs.GetInt("HighShadowsSave") == 1)
                    {
                        QualitySettings.shadows = ShadowQuality.All;
                    }

                    // shadow cascade

                    if (PlayerPrefs.GetInt("HighShadowCascadesSave") == 0)
                    {
                        QualitySettings.shadowCascades = 0;
                    }
                    else if (PlayerPrefs.GetInt("HighShadowCascadesSave") == 1)
                    {
                        QualitySettings.shadowCascades = 2;
                    }
                    else if (PlayerPrefs.GetInt("HighShadowCascadesSave") == 2)
                    {
                        QualitySettings.shadowCascades = 4;
                    }


                    // shadow distance

                    QualitySettings.shadowDistance = PlayerPrefs.GetFloat("HighShadowDistanceSave");

                    // anti-aliasing

                    QualitySettings.antiAliasing = PlayerPrefs.GetInt("HighAntiAliasingSave");

                    // texture resolution

                    if (PlayerPrefs.GetInt("HighSoftParticlesSave") == 0)
                    {
                        QualitySettings.softParticles = false;
                    }
                    else if (PlayerPrefs.GetInt("HighSoftParticlesSave") == 1)
                    {
                        QualitySettings.softParticles = true;
                    }
                }

                else if (i == 3)
                {

                    // shadow resolution
                    if (PlayerPrefs.GetInt("VeryHighShadowQualitySave") == 0)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.Low;
                    }
                    else if (PlayerPrefs.GetInt("VeryHighShadowQualitySave") == 1)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.Medium;
                    }
                    else if (PlayerPrefs.GetInt("VeryHighShadowQualitySave") == 2)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.High;
                    }
                    else if (PlayerPrefs.GetInt("VeryHighShadowQualitySave") == 3)
                    {
                        QualitySettings.shadowResolution = ShadowResolution.VeryHigh;
                    }

                    if (PlayerPrefs.GetInt("VeryHighShadowsSave") == 0)
                    {
                        QualitySettings.shadows = ShadowQuality.HardOnly;
                    }
                    else if (PlayerPrefs.GetInt("VeryHighShadowsSave") == 1)
                    {
                        QualitySettings.shadows = ShadowQuality.All;
                    }

                    // shadow cascade

                    if (PlayerPrefs.GetInt("VeryHighShadowCascadesSave") == 0)
                    {
                        QualitySettings.shadowCascades = 0;
                    }
                    else if (PlayerPrefs.GetInt("VeryHighShadowCascadesSave") == 1)
                    {
                        QualitySettings.shadowCascades = 2;
                    }
                    else if (PlayerPrefs.GetInt("VeryHighShadowCascadesSave") == 2)
                    {
                        QualitySettings.shadowCascades = 4;
                    }


                    // shadow distance

                    QualitySettings.shadowDistance = PlayerPrefs.GetFloat("VeryHighShadowDistanceSave");

                    // anti-aliasing

                    QualitySettings.antiAliasing = PlayerPrefs.GetInt("VeryHighAntiAliasingSave");

                    // texture resolution

                    if (PlayerPrefs.GetInt("VeryHighSoftParticlesSave") == 0)
                    {
                        QualitySettings.softParticles = false;
                    }
                    else if (PlayerPrefs.GetInt("VeryHighSoftParticlesSave") == 1)
                    {
                        QualitySettings.softParticles = true;
                    }
                }
            }

            QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("QualitySettings"), true);
        }
    }

    private void Update()
    {
        if (Save)
        {
            SaveSettings();
            Save = false;
        }
        
    }

    public void SaveSettings()
    {
        if (!PlayerPrefs.HasKey("Saves"))
        {
            PlayerPrefs.SetInt("Saves", 0);
        }
        else
        {
            PlayerPrefs.SetInt("Saves", PlayerPrefs.GetInt("Saves") + 1);
            PlayerPrefs.SetInt("QualitySettings", QualitySettings.GetQualityLevel());
            if (Screen.fullScreen)
            {
                PlayerPrefs.SetInt("WindowMode", 0);
            }
            else
            {
                PlayerPrefs.SetInt("WindowMode", 1);
            }

            PlayerPrefs.SetString("ResolutionSave", Screen.width + "X" + Screen.height);

            for (int i = 0; i < 4; i++)
            {
                QualitySettings.SetQualityLevel(i, true);
                if (i == 0)
                {

                    // shadow resolution
                    if (QualitySettings.shadowResolution == ShadowResolution.Low)
                    {
                        PlayerPrefs.SetInt("LowShadowQualitySave", 0);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.Medium)
                    {

                        PlayerPrefs.SetInt("LowShadowQualitySave", 1);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.High)
                    {

                        PlayerPrefs.SetInt("LowShadowQualitySave", 2);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.VeryHigh)
                    {

                        PlayerPrefs.SetInt("LowShadowQualitySave", 3);
                    }

                    // shadow
                    if (QualitySettings.shadows == ShadowQuality.HardOnly)
                    {

                        PlayerPrefs.SetInt("LowShadowsSave", 0);
                    }
                    else if (QualitySettings.shadows == ShadowQuality.All)
                    {

                        PlayerPrefs.SetInt("LowShadowsSave", 1);
                    }

                    // shadow cascade

                    if (QualitySettings.shadowCascades == 0)
                    {

                        PlayerPrefs.SetInt("LowShadowCascadesSave", 0);
                    }
                    else if (QualitySettings.shadowCascades == 2)
                    {

                        PlayerPrefs.SetInt("LowShadowCascadesSave", 1);
                    }
                    else if (QualitySettings.shadowCascades == 4)
                    {

                        PlayerPrefs.SetInt("LowShadowCascadesSave", 2);
                    }


                    // shadow distance

                    PlayerPrefs.SetFloat("LowShadowDistanceSave", QualitySettings.shadowDistance);

                    // anti-aliasing

                    PlayerPrefs.SetInt("LowAntiAliasingSave", QualitySettings.antiAliasing);


                    // soft particles

                    if (QualitySettings.softParticles == false)
                    {
                        PlayerPrefs.SetInt("LowSoftParticlesSave", 0);
                    }
                    else if (QualitySettings.softParticles == true)
                    {
                        PlayerPrefs.SetInt("LowSoftParticlesSave", 1);
                    }
                }

                else if (i == 1)
                {

                    /// shadow resolution
                    if (QualitySettings.shadowResolution == ShadowResolution.Low)
                    {
                        PlayerPrefs.SetInt("MediumShadowQualitySave", 0);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.Medium)
                    {

                        PlayerPrefs.SetInt("MediumShadowQualitySave", 1);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.High)
                    {

                        PlayerPrefs.SetInt("MediumShadowQualitySave", 2);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.VeryHigh)
                    {

                        PlayerPrefs.SetInt("MediumShadowQualitySave", 3);
                    }

                    // shadow
                    if (QualitySettings.shadows == ShadowQuality.HardOnly)
                    {

                        PlayerPrefs.SetInt("MediumShadowsSave", 0);
                    }
                    else if (QualitySettings.shadows == ShadowQuality.All)
                    {

                        PlayerPrefs.SetInt("MediumShadowsSave", 1);
                    }

                    // shadow cascade

                    if (QualitySettings.shadowCascades == 0)
                    {

                        PlayerPrefs.SetInt("MediumShadowCascadesSave", 0);
                    }
                    else if (QualitySettings.shadowCascades == 2)
                    {

                        PlayerPrefs.SetInt("MediumShadowCascadesSave", 1);
                    }
                    else if (QualitySettings.shadowCascades == 4)
                    {

                        PlayerPrefs.SetInt("MediumShadowCascadesSave", 2);
                    }


                    // shadow distance

                    PlayerPrefs.SetFloat("MediumShadowDistanceSave", QualitySettings.shadowDistance);

                    // anti-aliasing

                    PlayerPrefs.SetInt("MediumAntiAliasingSave", QualitySettings.antiAliasing);


                    // soft particles

                    if (QualitySettings.softParticles == false)
                    {
                        PlayerPrefs.SetInt("MediumSoftParticlesSave", 0);
                    }
                    else if (QualitySettings.softParticles == true)
                    {
                        PlayerPrefs.SetInt("MediumSoftParticlesSave", 1);
                    }
                }

                else if (i == 2)
                {
                    /// shadow resolution
                    if (QualitySettings.shadowResolution == ShadowResolution.Low)
                    {
                        PlayerPrefs.SetInt("HighShadowQualitySave", 0);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.Medium)
                    {

                        PlayerPrefs.SetInt("HighShadowQualitySave", 1);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.High)
                    {

                        PlayerPrefs.SetInt("HighShadowQualitySave", 2);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.VeryHigh)
                    {

                        PlayerPrefs.SetInt("HighShadowQualitySave", 3);
                    }

                    // shadow
                    if (QualitySettings.shadows == ShadowQuality.HardOnly)
                    {

                        PlayerPrefs.SetInt("HighShadowsSave", 0);
                    }
                    else if (QualitySettings.shadows == ShadowQuality.All)
                    {

                        PlayerPrefs.SetInt("HighShadowsSave", 1);
                    }

                    // shadow cascade

                    if (QualitySettings.shadowCascades == 0)
                    {

                        PlayerPrefs.SetInt("HighShadowCascadesSave", 0);
                    }
                    else if (QualitySettings.shadowCascades == 2)
                    {

                        PlayerPrefs.SetInt("HighShadowCascadesSave", 1);
                    }
                    else if (QualitySettings.shadowCascades == 4)
                    {

                        PlayerPrefs.SetInt("HighShadowCascadesSave", 2);
                    }


                    // shadow distance

                    PlayerPrefs.SetFloat("HighShadowDistanceSave", QualitySettings.shadowDistance);

                    // anti-aliasing

                    PlayerPrefs.SetInt("HighAntiAliasingSave", QualitySettings.antiAliasing);


                    // soft particles

                    if (QualitySettings.softParticles == false)
                    {
                        PlayerPrefs.SetInt("HighSoftParticlesSave", 0);
                    }
                    else if (QualitySettings.softParticles == true)
                    {
                        PlayerPrefs.SetInt("HighSoftParticlesSave", 1);
                    }
                }

                else if (i == 3)
                {
                    /// shadow resolution
                    if (QualitySettings.shadowResolution == ShadowResolution.Low)
                    {
                        PlayerPrefs.SetInt("VeryHighShadowQualitySave", 0);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.Medium)
                    {

                        PlayerPrefs.SetInt("VeryHighShadowQualitySave", 1);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.High)
                    {

                        PlayerPrefs.SetInt("VeryHighShadowQualitySave", 2);
                    }
                    else if (QualitySettings.shadowResolution == ShadowResolution.VeryHigh)
                    {

                        PlayerPrefs.SetInt("VeryHighShadowQualitySave", 3);
                    }

                    // shadow
                    if (QualitySettings.shadows == ShadowQuality.HardOnly)
                    {

                        PlayerPrefs.SetInt("VeryHighShadowsSave", 0);
                    }
                    else if (QualitySettings.shadows == ShadowQuality.All)
                    {

                        PlayerPrefs.SetInt("VeryHighShadowsSave", 1);
                    }

                    // shadow cascade

                    if (QualitySettings.shadowCascades == 0)
                    {

                        PlayerPrefs.SetInt("VeryHighShadowCascadesSave", 0);
                    }
                    else if (QualitySettings.shadowCascades == 2)
                    {

                        PlayerPrefs.SetInt("VeryHighShadowCascadesSave", 1);
                    }
                    else if (QualitySettings.shadowCascades == 4)
                    {

                        PlayerPrefs.SetInt("VeryHighShadowCascadesSave", 2);
                    }


                    // shadow distance

                    PlayerPrefs.SetFloat("VeryHighShadowDistanceSave", QualitySettings.shadowDistance);

                    // anti-aliasing

                    PlayerPrefs.SetInt("VeryHighAntiAliasingSave", QualitySettings.antiAliasing);


                    // soft particles

                    if (QualitySettings.softParticles == false)
                    {
                        PlayerPrefs.SetInt("VeryHighSoftParticlesSave", 0);
                    }
                    else if (QualitySettings.softParticles == true)
                    {
                        PlayerPrefs.SetInt("VeryHighSoftParticlesSave", 1);
                    }

                }
            }
            QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("QualitySettings"), true);
        }
    }
}