﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System;
using VRTK.Controllables.PhysicsBased;
using TMPro;

public class LockUnlockWithPin : MonoBehaviour
{
    [SerializeField] GameObject doors;
    [SerializeField] int keyLenght = 0;
    [SerializeField] int[] customKey;
    [SerializeField] GameObject[] keyObjects;
    [SerializeField] TimeLeft timer;
    [SerializeField] GroundControll ground;
    [SerializeField] float MinusTimeBad;
    [SerializeField] AccessGranted access;
    AccessDenied accessDenied;
    VRTK_PhysicsRotator rotator;
    string tempTMPRO = "";
    StringBuilder stringBuild;
    TextMeshPro text;
    bool open = false;
    bool fail = true;
    bool equals = true;
    int count = 0;
    int countCheck = 0 ;
    int[] key;
    int[] playerNumbers;
    bool firstTime = true;

    bool correct = false;
    // Start is called before the first frame update
    void Start()
    {
        stringBuild = new StringBuilder();
        rotator = doors.GetComponent<VRTK_PhysicsRotator>();
        text = GetComponent<TextMeshPro>();
        accessDenied = GetComponent<AccessDenied>();
        rotator.enabled = false;
        if (customKey.Length == 0)
        {
            key = new int[keyLenght];
            playerNumbers = new int[keyLenght];
            for (int i = 0; i < keyLenght; i++)
            {
                key[i] = UnityEngine.Random.Range(0, 9);
                keyObjects[i].GetComponentInChildren<TextMeshPro>().text = key[i].ToString();
                Debug.Log(key[i]);
            }
        }
        else
        {
            keyLenght = customKey.Length;
            key = customKey;
            playerNumbers = new int[keyLenght];
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        if (firstTime)
        {
            firstTime = false;
        }
        if (countCheck < keyLenght)
        {
            if (key[countCheck] != playerNumbers[countCheck])
            {
                equals = false;
            }
            
        }
        countCheck++;

        if (equals && count == keyLenght && countCheck >= keyLenght)
        {
            rotator.enabled = true;
            text.text = "Access Granted";
            timer.StopTimer();
            timer.finalStop = true;
            GameData.SetTime((int)Math.Truncate(timer.timerTime));
            GameData.SetEnd(true);
            GameData.SetVictory(true);
            ground.finishOn();
            open = true;
        }
        if (!equals && count == keyLenght && !open && countCheck >= keyLenght)
        {
            text.text = "Access Denied";
            accessDenied.StartSound();
            timer.MinusTime(MinusTimeBad);
            count = 0;
            playerNumbers = new int[keyLenght];
            fail = true;
        }

        if (countCheck >= keyLenght && !equals)
        {
            countCheck = 0;
            equals = true;
        }

        if(open && !correct)
        {
            correct = true;
            access.PlaySound();
        }

    }

    public void SetNumber(int val)
    {
        if (count < keyLenght)
        {

            if (fail)
            {
                stringBuild = new StringBuilder();
                stringBuild.Append(tempTMPRO);
                text.text = stringBuild.ToString();
                fail = false;

            }
            if (!open && count >= 0)
            {
                playerNumbers[count] = val;
                stringBuild.Append(" " + val);
                text.text = stringBuild.ToString();
                count++;
            }
            else if (!open && count < 0)
            {
                count = 0;
                playerNumbers[count] = val;
                stringBuild.Append(" " + val);
                text.text = stringBuild.ToString();
                count++;
            }
        }

    }

    public void Del()
    {
        if (!open && count >= 0)
        {
            if (count == 0)
            {
                stringBuild.Remove(stringBuild.Length - 1, 1);
                text.text = stringBuild.ToString();
            }
            else
            {
                stringBuild.Remove(stringBuild.Length - 2, 2);
                text.text = stringBuild.ToString();
            }
            count--;
            if (count < 0)
            {
                stringBuild = new StringBuilder();
                stringBuild.Append(tempTMPRO);
                text.text = stringBuild.ToString();
            }
        }
    }
}
