﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SmashSound : MonoBehaviour
{
    BottleSmash smash;
    bool smashed = false;
    AudioSource aud;
    // Start is called before the first frame update
    void Start()
    {
        smash = GetComponentInParent<BottleSmash>();
        aud = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (smash.broken && !smashed)
        {
            aud.Play();
            smashed = true;
        }
    }
}
