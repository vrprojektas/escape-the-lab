﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LighterFire : MonoBehaviour
{
    AudioSource audio;
    bool Fire = true;
    FireOn fireScript;
    float time;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        fireScript = GetComponentInParent<FireOn>();
        time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (fireScript.Active && Fire)
        {
            audio.Play();
            Fire = false;
        }
        else if (!fireScript.Active && !Fire && fireScript.time > time)
        {
            time += Time.deltaTime;
        }
        else if (!fireScript.Active && !Fire && fireScript.time <= time)
        {
            audio.Stop();
            Fire = true;
            time = 0;
        }
        
    }
}
